
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Kuture</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript"></script>
  
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbarIndex navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="imagens/logos/logo4.2.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navIndex navbar-right">
        <li><a href="#sobre">SOBRE</a></li>
        <li><a href="#ferramenta">FERRAMENTA</a></li>
        <li><a href="#contato">CONTATO</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle menuDrop" data-toggle="dropdown" href="#">EVENTOS
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="dropdown-header">FESTIVAIS</li>
              <li><a href="festivais.php?ref=1">DANÇA</a></li>
              <li><a href="festivais.php?ref=2">MÚSICA</a></li>
              <li><a href="festivais.php?ref=3">TEATRO</a></li>
              <li><a href="festivais.php?ref=4">OUTROS</a></li>
              <li role="separator" class="divider"></li>
            <li class="dropdown-header">SHOWS</li>
              <li><a href="shows.php?ref=1">AXÉ</a></li>
              <li><a href="shows.php?ref=2">BOSSA NOVA</a></li>
              <li><a href="shows.php?ref=3">FORRÓ</a></li> 
              <li><a href="shows.php?ref=4">FUNK</a></li>
              <li><a href="shows.php?ref=5">GOLSPEL</a></li>
              <li><a href="shows.php?ref=6">HIP HOP</a></li>
              <li><a href="shows.php?ref=7">JAZZ</a></li>
              <li><a href="shows.php?ref=8">MPB</a></li>
              <li><a href="shows.php?ref=9">PAGODE</a></li>
              <li><a href="shows.php?ref=10">RAP</a></li>
              <li><a href="shows.php?ref=11">REGGAE</a></li>
              <li><a href="shows.php?ref=12">ROCK</a></li>
              <li><a href="shows.php?ref=13">SAMBA</a></li>
              <li><a href="shows.php?ref=14">SERTANEJO</a></li>
              <li><a href="shows.php?ref=15">Outros</a></li>
          </ul>
        </li>
        <li><a href="cadastro.php">CADASTRE-SE</a></li>
        <li id="myLogin" style="cursor:pointer;cursor:hand;"><a><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
      </ul>
    </div>
  </div>
</nav>


<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      
      <div class="item active">
        <img src="imagens/carousel/01.jpg" alt="New York">
        <div class="carousel-caption">
          <h3>Amantes de Música</h3>
          <p>Principalmente as de Melhor qualidade</p>
        </div>      
      </div>

      <div class="item">
        <img src="imagens/carousel/02.jpg" alt="Chicago">
        <div class="carousel-caption">
          <h3>Avisaremos a todos</h3>
          <p>Nossa ferramenta irá permitir que você avise todo mudo sobre seu evento.</p>
        </div>      
      </div>
    
      <div class="item">
        <img src="imagens/carousel/03.jpg" alt="Los Angeles">
        <div class="carousel-caption">
          <h3>Inesquecível</h3>
          <p>Tornaremos seu evento impossível de esquecer.</p>
        </div>      
      </div>

      <div class="item">
        <img src="imagens/carousel/04.jpg" alt="Los Angeles">
        <div class="carousel-caption">
          <h3>Música de Qualidade</h3>
          <p>Nada melhor do que ouvir aquilo que gostamos.</p>
        </div>      
      </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Proxima</span>
    </a>
</div>

<!-- Modal -->
  <div class="modal fade" id="modalLogin" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="modalTitulo"><span class="glyphicon glyphicon-home"></span> Bem Vindo!</h4>
          <p style="font-size: 12pt;">Área do Usuário Kulture.</p>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <div class="divImgModal">
            <img id="imgModal" src="imagens/logos/logo2.png">
          </div>
          <form role="form" id="formLogin">
            <div class="form-group">
              <label for="username"><span class="glyphicon glyphicon-user"></span> Usuário</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Informe seu Login">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Senha</label>
              <input type="password" class="form-control" id="psw" name="psw" placeholder="Informe sua senha">
            </div>
              <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Acessar</button>
              <div id="divGif"><img  id="loginGif" src="imagens/gif/load.gif" style="max-width: 50px;"></div>
              <p class="text-center" id="erroLogin"></p>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
          <p>Não é cadastrado?<a href="cadastro.php">Registre-se.</a></p>
          <!--p>Esqueceu a <a href="#">Senha?</a></p-->
        </div>
      </div>
      
    </div>
  </div> 


<!-- Container (sobre Section) -->
<div id="sobre" class="container-fluid">
  <div class="row">
    <div class="col-sm-8">
      <h2>Quem Somos</h2>
      <h4 align="justify" class="recuo">Kulture é uma plataforma completa para inscrições de eventos para divulgação de shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. </h4>
      <a href="#contact" class="btn btn-default btn-lg btnEntraEmContato">Entrar em Contato</a>
    </div>
    <div class="col-sm-4">
      <div class="iconeCentralizado">
        <span class="glyphicon glyphicon-headphones icone-maior"></span>  
      </div>
      
    </div>
  </div>
</div>

<div class="container-fluid fundo_cinza">
  <div class="row">
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-volume-down icone-medio esconderParaDeslizar"></span></br></br></br></br>
      <span class="glyphicon glyphicon-volume-up icone-medio esconderParaDeslizar esconderMobile"></span>
    </div>
    <div class="col-sm-8">
      <br>
      <h4 align="justify"><strong>MISSÃO:</strong> Facilitar o trabalho do profissional de eventos que deseja divuldar seus grandes eventos no recife ou de pessoas que gostariam de começar a divulgar seus eventos de uma forma gratuita e com grande poder de alcance. Proporcionamos esta ferramenta para isso.</h4><br>
      <p align="justify"><strong>VISÃO:</strong> Buscamos atingir a todos. Desde grandes eventos à pequenos grupos que não tinham como divulgar seu trabalho, e gostariam de atingir um grande número de pessoas mas não tinham como. Apoiamos a cultura e a boa música.</p>
    </div>
  </div>
</div>

<!-- Container (ferramenta Section) -->
<div id="ferramenta" class="container-fluid esconderParaDeslizar">
  <h2 class="text-center">FERRAMENTA</h2>
  <h4 class="text-center">O que ela realiza</h4>
  <br>
  <div class="row">
    <div class="col-sm-8">
      <h4 align="justify" class="recuo">Amamos nossa cidade, apoiamos a cultura e sabemos que muitos eventos e shows que são realizados nela poderiam ter a presença de mais pessoas se houvesse a divulgação correta. Então basicamente fazemos essa divulgação para você. Não importa o tamanho do seu evento.</h4>
      <h4 align="justify" class="recuo">Existem duas formas de utilizar nosso sistema, uma para <strong>divulgadores</strong> de eventos e outra para os <strong>apoiadores</strong> dos eventos.</h4>
      <h4 align="justify" class="recuo"><strong>DIVULGADOR:</strong> Ao se cadastradar no site, você poderá criar eventos, que por sua vez serão visualizados em suas respectivas páginas de acordo com a sua classificação. Antes de publicados os eventos serão analisados por nossa equipe. Para ver se estão de acordo com nossa política de publicações.</h4>
       <h4 align="justify" class="recuo"><strong>APOIADOR:</strong> Em sua guia os apoiadores poderão seguir estilos cadastrados em nosso sistema. Ao seguir um estilo, um alerta será enviado sempre que um novo evento para aquele estilo for criado. Basicamente se você é um apoiador e gostaria apenas de receber alertas sobre eventos de pagode, basta apenas escolher esta opção nas configurações que apenas alertas sobre eventos de pagode serão enviados até você. Os alertas são através de E-Mail e SMS. Podendo ser cancelados a qualquer momento.</h4>
       <a href="cadastro.php" class="btn btn-default btn-lg btnEntraEmContato">Cadastre-se</a>
      

    </div>
    <div class="col-sm-4">
      <div class="iconeCentralizado">
        <span class="glyphicon glyphicon-bullhorn icone-maior"></span>
      </div>
      
    </div>
  </div>
</div>

<div class="container-fluid recifeBack esconderParaDeslizar">
  <img src="imagens/carousel/recife.jpg">
</div>

<!-- Container (contato Section) -->
<div id="contato" class="container-fluid fundo_cinza">
  <h2 class="text-center">CONTATO</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Caso você tenha alguma dúvida entre em contato<br>Falaremos com você em 24hs.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Recife, PE</p>
      <p><span class="glyphicon glyphicon-phone"></span> 53000-000</p>
      <p><span class="glyphicon glyphicon-envelope"></span> contato@.com.br</p>
    </div>
    <div class="col-sm-7 esconderParaDeslizar">
      <form id="formularioContato">
      <fieldset id="fieldsetContato">
      <div class="row">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="nome" name="nome" placeholder="Nome" type="text" required><span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="E-mail" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="exemplo@exemplo.com" required><span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span>
        </div>
      </div>
      <textarea class="form-control" id="comentario" name="comentario" placeholder="Comentário" rows="5"></textarea><span class='glyphicon glyphicon-ok form-control-feedback imputForm'></span><br>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit">Enviar</button>
          <div id="divGif"><img  id="loadGif" src="imagens/gif/load.gif" style="max-width: 50px;"></div>
          <p class="text-center animated bounceInRight" id="sucessoEnvio"><span class="glyphicon glyphicon-ok"></span><strong> Recebemos seu contato, agora é so aguardar que retornaremos.</strong></p>
          <p class="text-center" id="erroEnvio"></p>
        </div>
      </div>
      </fieldset>
      </form>
    </div>
  </div>
</div>

<!-- Add Google Maps -->
<div id="googleMap" style="height:400px;width:100%;"></div>
<script>
function myMap() {
var myCenter = new google.maps.LatLng(-8.0475622,-34.8831583,15);
var mapProp = {
  center:myCenter,
  zoom:15,
  scrollwheel:false,
  draggable:false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
};

var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
var marker = new google.maps.Marker({position:myCenter});
marker.setMap(map);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBd6RFqG28JTPcJK1534tg2O0PeZrgIPWs&callback=myMap"></script>

<!-- >AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU //CHAVE DA W3-->



<!--
Usar api do google
Ver em: https://www.w3schools.com/graphics/google_maps_basic.asp
-->

<?php require_once ("footer.php"); ?>
<script async src="js/main.js"></script>

</body>
</html>
