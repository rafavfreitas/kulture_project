<?php include '../db/timelineFestPendentes.php';?>
<?php require_once ("validaSessaoAdmin.php"); ?>
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Admnistrador</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="../imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript"></script>
  
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbarIndex navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="../imagens/logos/logo4.2.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navIndex navbar-right">
        <li><a href="index.php">HOME</a></li>
        <li><a href="shows.php">SHOWS PENDENTES</a></li>
        <li class="active"><a href="festivais.php">FESTIVAIS PENDENTES</a></li>
        <li><a href="conta.php">MINHA CONTA</a></li>
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Sair</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron jumbIndexShows text-center">
    <img id="logo_maior" class="animated fadeIn" src="../imagens/logos/logo1.1.png" style="max-width: 50px;">
    <p id="textoLogo" class="animated fadeInUp">Amamos a música</p> 

</div>




<div class="container-fluid">
  <!--h2><?php echo $estilo_nome; ?></h2-->
    <h3>Verifique os últimos Festivais cadastrados</h3>
    <p><strong>Nota:</strong> Você pode autorizar a publicação ou apagá-lo da ferramenta.</p>
    
    
  
</div>

<div class="container containerViewShows">
<p class="text-center"><img  id="loadPublicacao" src="../imagens/gif/load1.gif" style="max-width: 100px; display:scroll;position:fixed;bottom: 0px;right:0px;"></p>

<?php 
  try {
    foreach ($pdo->query($sql) as $row) {?>

    <div class="panel panel-default panelViewShowDefault">
      <div class="panel-heading panelViewShowHeading">
      <h3 class="text-center" style="font-size: 18pt;" class="panelViewShowTitulo"><?php echo $row['fest_nome']; ?></h3>
      <?php echo "<img src='../imagens/img_eventos/festivais/".$row['fest_id'].".jpg' class='img-responsive' style='margin-right: auto; margin-left: auto';>" ?>
      
      </div>

      <div class="panel-body">
      
      <div class="row">
        <div class="col-sm-6">
          <dl>
            <dt>Nome:</dt>
            <dd><?php echo $row['fest_nome']; ?></dd>
            <dt>Data Show:</dt>
            <dd><?php echo $row['fest_data']; ?></dd>
            <dt>Local:</dt>
            <dd><?php echo $row['fest_local']; ?></dd>
          </dl>
        </div>
        <div class="col-sm-6">
          <dl>
            <dt>Contato:</dt>
            <dd><?php echo $row['fest_contato']; ?></dd>
            <dt>Preço:</dt>
            <dd><?php echo $row['fest_preco']; ?></dd>
            <dt>Publicado na ferramenta:</dt>
            <dd><?php echo $row['fest_publicado']; ?></dd>
          </dl>
        </div>
      </div>
       <p class="text-center"><?php echo '<button type="button" class="btn btn-success" id="liberar" value="'.$row['fest_id'].'">Publicar</button>'; ?></p>
      <p class="text-center"><?php echo '<button type="button" class="btn btn-danger" id="deletar" value="'.$row['fest_id'].'">Negar</button>'; ?></p>
      </div>
    </div>
    <hr>
    <?php
    }
        } catch (Exception $e) {
          //echo 'ERROR: ' . $e->getCode() .'Formação das Linhas';
          echo $e;
        }?>


 
    <?php
                            //Verificar a pagina anterior e posterior
    $pagina_anterior = $pagina - 1;
    $pagina_posterior = $pagina + 1;
    ?>
                       
                        <nav class="text-center">
                                <ul class="pagination">
                                  <li>
                                    <?php
                                    if($pagina_anterior != 0){ ?>
                                    <a href="festivais.php?pagina=<?php echo $pagina_anterior; ?>" aria-label="Previous">
                                      <span aria-hidden="true">&laquo;</span>
                                    </a>
                                    <?php }else{ ?>
                                    <span aria-hidden="true">&laquo;</span>
                                    <?php }  ?>
                                  </li>
                                  <?php 
                                    //Apresentar a paginacao
                                  for($i = 1; $i < $num_pagina + 1; $i++){ 
                                    if ($i == $pagina) { ?>
                                    <li class="active"><a href="festivais.php?pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                    <?php }else{ ?>
                                    <li><a href="festivais.php?pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                    <?php } }?>
                                    <li>
                                      <?php
                                      if($pagina_posterior <= $num_pagina){ ?>
                                      <a href="festivais.php?pagina=<?php echo $pagina_posterior; ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                      </a>
                                      <?php }else{ ?>
                                      <span aria-hidden="true">&raquo;</span>
                                      <?php }  ?>
                                    </li>
                                  </ul>
                                </nav>



                            

</div>



<?php require_once ("footer.php"); ?>
<script async src="js/principal.js"></script>
<script async src="js/manterFest.js"></script>

</body>
</html>
