$(document).ready(function(){
        $('#loadAtualizarUser').hide();
        $('#sucessoAtualizarRoot').hide();
        $('#erroAtualizarRoot').hide();
            $('#formAtualizarUser').submit(function(){  //Ao submeter formulário
                $('#loadAtualizarUser').show();
                $('#sucessoAtualizarRoot').hide();
                $('#erroAtualizarRoot').hide();
                var formData = new FormData($(this)[0]);
                $.ajax({            //Função AJAX
                    url:"../db/manterRoot.php",
                    type: 'POST',
                    data: formData,
                    async: false,                    //Arquivo php
                    success: function (result){             //Sucesso no AJAX
                        if(result==1){
                            $('#loadAtualizarUser').hide();
                            $('#fieldsetAtualizarUser').attr("disabled", "disabled");
                            $('#sucessoAtualizarRoot').show();
                            $('#sucessoAtualizarRoot').addClass('animated shake');                   
                        }if (result !=1 ){
                            $('#loadAtualizarUser').hide();
                            $("#erroAtualizarRoot").html("<p class='text-center'>Desculpe: "+ result+ "</p>");
                            $('#erroAtualizarRoot').show();
                            $('#erroAtualizarRoot').addClass('animated shake');
                        }  
                    },
                    
                    cache: false,
                    contentType: false,
                    processData: false
                });
            return false;   //Evita que a página seja atualizada
            });
});





$(document).ready(function(){
  // Código para adicionar suavidade no deslizamento da página através do menu.
  $(".navbar a, footer a[href='#myPage'], .btnEntraEmContato").on('click', function(event) {
    //Certifique-se de que this.hash tem um valor antes de substituir o comportamento padrão
    if (this.hash !== "") {
      // Prevenir o comportamento de clique de âncora padrão
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Usando o método jQuery animate () para adicionar scroll de página suave
      // Um número opcional (900) que significa o número de milissegundos necessários para rolar para a área escolhida.
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Adicionar hash(#) ao URL quando feito scroll (comportamento de clique padrão)
        window.location.hash = hash;
      });
    } // Fim do IF
  });
});


function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)
  
  if (texto.substring(0,1) != saida){
     documento.value += texto.substring(0,1);
  }
  
}