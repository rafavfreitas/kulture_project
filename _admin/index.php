<?php include '../db/timelineShowsPendentes.php';?>
<?php require_once ("validaSessaoAdmin.php"); ?>
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Admnistrador</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="../imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript"></script>
  
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbarIndex navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="../imagens/logos/logo4.2.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navIndex navbar-right">
        <li class="active"><a href="index.php">HOME</a></li>
        <li><a href="shows.php">SHOWS PENDENTES</a></li>
        <li><a href="festivais.php">FESTIVAIS PENDENTES</a></li>
        <li><a href="conta.php">MINHA CONTA</a></li>
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Sair</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron jumbIndexShows text-center">
    <img id="logo_maior" class="animated fadeIn" src="../imagens/logos/logo1.1.png" style="max-width: 50px;">
    <p id="textoLogo" class="animated fadeInUp">Amamos a música</p> 

</div>




<div class="container-fluid">
  <!--h2><?php echo $estilo_nome; ?></h2-->
    <h3 class="text-center">Bem vindo a área administrativa do sistema</h3>
    <p class="text-center"><strong>Nota:</strong> Escolha as opções do Menu acima</p>
    
    
  
</div>

<div class="container containerViewShows">



                            

</div>



<?php require_once ("footer.php"); ?>
<script async src="js/principal.js"></script>

</body>
</html>
