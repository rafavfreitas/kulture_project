<?php include '../db/timelineShowsPendentes.php';?>
<?php require_once ("validaSessaoAdmin.php"); ?>
<?php include '../db/consultaRoot.php'; ?>
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Admnistrador</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="../imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript"></script>
  
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbarIndex navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="../imagens/logos/logo4.2.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navIndex navbar-right">
        <li><a href="index.php">HOME</a></li>
        <li><a href="shows.php">SHOWS PENDENTES</a></li>
        <li><a href="festivais.php">FESTIVAIS PENDENTES</a></li>
        <li class="active"><a href="conta.php">MINHA CONTA</a></li>
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Sair</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron jumbIndexShows text-center">
    <img id="logo_maior" class="animated fadeIn" src="../imagens/logos/logo1.1.png" style="max-width: 50px;">
    <p id="textoLogo" class="animated fadeInUp">Amamos a música</p> 

</div>




<div class="container-fluid">
  
    
    
  
</div>

<div class="container containerViewShows">

  <div class="panel panel-default panelCadastro" style="max-width: 700px; margin-right: auto;margin-left: auto;">
    <div class="panel-heading panelCadastroHeading">
      <h4 id="cadastroTitulo" class="text-center">
        <span class="glyphicon glyphicon-ok iconCadasto"></span> Seus Dados!
      </h4>
      <p class="text-center"><span class="glyphicon glyphicon-list-alt iconCadasto"></span> Para caso você deseje atualizá-los.</p>
    </div>

     <div class="divGifCadastro">
            <img id="gifCadastro" src="../imagens/gif/equalizer.gif">
          </div>

      <form class="form-horizontal" id="formAtualizarUser">
        <fieldset id="fieldsetAtualizarUser">
          <!--Nome-->
        <div class="form-group">
            <label class="control-label col-sm-2" for="nome">Nome:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe seu nome" required value=<?php echo "'$user_nome'"; ?> >
            </div>
        </div>

          <!--E-Mail-->
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">E-Mail:</label>
            <div class="col-sm-10"> 
              <input type="email" class="form-control" id="email" name="email" placeholder="E-mail para contato." pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="exemplo@exemplo.com" required value=<?php echo "'$user_email'"; ?>>
            </div>
          </div>

          <!--Telefone-->
          <div class="form-group">
            <label class="control-label col-sm-2" for="telefone">Telefone:</label>
            <div class="col-sm-10">
              <input pattern="^\d{2}-\d{5}-\d{4}$" type="tel" class="form-control" rows="3" id="telefone" name="telefone" OnKeyPress="formatar('##-#####-####', this)" maxlength="13" placeholder="00-00000-0000" style="max-width: 200px;" required value=<?php echo "'$user_telefone'"; ?>></input>
              Caso deseje informar um Nº Fixo, colocar um 0 no lugar do 9º dígito.
            </div>
          </div>

            <!--Login-->
          <div class="form-group">
              <label class="control-label col-sm-2" for="login">Login:</label>
              <div class="col-sm-10"> 
                <input type="text" class="form-control" id="login" name="login" placeholder="Informe um Login de acesso" required value=<?php echo "'$user_login'"; ?>>
             
              </div>
          </div>
          <!--Senha-->
          <div class="form-group">
            <label class="control-label col-sm-2" for="senha">Senha:</label>
            <div class="col-sm-10">
              <input type="checkbox"  onchange="document.getElementById('senha').disabled = !this.checked;">
                Para habilitar marque a caixa ao lado.
              <input type="password" class="form-control" id="senha" name="senha" placeholder="Informe sua senha" disabled required>
              
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="nome">Data Nasc.</label>
            <div class="col-sm-10">
            <p><?php echo "'$user_datanasc'"; ?></p>
             <input type="checkbox"  onchange="document.getElementById('datenasc').disabled = !this.checked;">
             Para alterar marque a caixa ao lado.
            <input class="form-control" type="date" id="datenasc" name="datenasc" style='max-width: 180px;' required disabled>
            
            </div>
          </div>
          

          <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Atualizar</button>
                <img  id="loadAtualizarUser" src="../imagens/gif/load1.gif" style="max-width: 50px;">
            </div>
          </div>
        <p class="text-center animated bounceInRight" id="sucessoAtualizarRoot"><span class="glyphicon glyphicon-ok"></span><strong> Dados atualizados com sucesso.</strong></p>
          <p class="text-center" id="erroAtualizarRoot"></p><br>
          </fieldset>
      </form>

  </div>



                            

</div>



<?php require_once ("footer.php"); ?>
<script async src="js/principal.js"></script>

</body>
</html>
