<?php
  
session_start();  //A seção deve ser iniciada em todas as páginas
if (!isset($_SESSION['UsuarioID']) || !isset($_SESSION['UsuarioNome']) || !isset($_SESSION['UsuarioNivel'])  ) {
    session_destroy();            //Destroi a seção por segurança
    header("Location: index.php"); exit;  //Redireciona o visitante para login
}else {
  if ($_SESSION['UsuarioNivel'] == 0) {
    header("Location: /_system/index.php"); exit;
  }elseif ($_SESSION['UsuarioNivel'] == 1) {
    header("Location: /_admin/index.php"); exit;
  }
  
}

?>

