
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Kuture</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="../imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <?php require_once ("validaSessao.php"); ?>
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbarIndex navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="../imagens/logos/logo4.2.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navIndex navbar-right">
        <li class="active"><a href="index.php">HOME</a></li>
        <li><a href="shows.php">MEUS SHOWS</a></li>
        <li><a href="festivais.php">MEUS FESTIVAIS</a></li>
        <li><a href="conta.php">MINHA CONTA</a></li>
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Sair</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron jumbIndex text-center">
  <img id="logo_maior" class="animated fadeIn" src="../imagens/logos/logo1.1.png">
  <p id="textoLogo" class="animated fadeInUp">Amamos a música</p> 

  <!--form>
    <div class="input-group">
      <input type="email" class="form-control" size="50" placeholder="Email Address" required>
      <div class="input-group-btn">
        <button type="button" class="btn btn-danger"></button>
      </div>
    </div>
  </form-->
</div>


<!-- Container (sobre Section) -->
<div id="index" class="container-fluid">
  
    <h2>Eventos</h2>
    <h3>Escolha o evento que deseja cadastrar</h3>
    <p><strong>Nota:</strong> Ao clicar na opção desejada seu conteúdo será exibido.</p>
    <div class="panel-group" id="accordion">
      
      <div class="panel panel-default panel-shows">
        <div class="panel-heading tituloshows" data-toggle="collapse" data-target="#collapse1" data-parent="#accordion" onclick="mudarIconeEventos('#collapse1','.setaAccordion1')">
          <h3 class="panel-title text-center">Shows
            <p class="textoAccordion setaAccordion1"><span class="glyphicon glyphicon-menu-up iconeAccordion"></span></p>
          </h3>
        </div>
        <div id="collapse1" class="panel-collapse coll-exame collapse in">
          <div class="panel-body">
          

            <form class="form-horizontal" id="formCadastroShow" enctype="multipart/form-data">
              <fieldset id="fieldsetCadastroShow">
                <!--Nome-->
              <div class="form-group addSuccessShow">
                  <label class="control-label col-sm-2" for="nome_show">Nome:</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nome_show" name="nome_show" placeholder="Informe o nome do Show" required>
                  </div>
              </div>

                <!--Data-->
                <div class="form-group addSuccessShow">
                  <label class="control-label col-sm-2" for="date_show">Data:</label>
                  <div class="col-sm-10">
                  <input class="form-control" type="datetime-local" id="date_show" name="date_show" style='max-width: 220px;' required>Informe a data e a hora do Show.
                  </div>
                </div>

                <!--Endereço-->
                <div class="form-group addSuccessShow">
                  <label class="control-label labelForm col-sm-2" for="local_show">Local:</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="local_show" name="local_show" placeholder="Informe o endereço do Show" required></textarea>
                  </div>
                </div>

                <!--Telefone-->
                <div class="form-group addSuccessShow">
                  <label class="control-label col-sm-2" for="telefone_show">Telefone:</label>
                  <div class="col-sm-10">
                    <input pattern="^\d{2}-\d{5}-\d{4}$" type="tel" class="form-control" rows="3" id="telefone_show" name="telefone_show" OnKeyPress="formatar('##-#####-####', this)" maxlength="13" placeholder="00-00000-0000" style="max-width: 200px;" required></input>
                    Informe um telefone para contato.<br>Caso deseje informar um Nº Fixo, colocar um 0 no lugar do 9º dígito.
                  </div>
                </div>

                  <!--Preços-->
                <div class="form-group addSuccessShow">
                  <label class="control-label labelForm col-sm-2" for="precos_show">Preços:</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="precos_show" name="precos_show" placeholder="Descreva informações sobre preços do Show" required></textarea>
                  </div>
                </div>

                <div class="form-group addSuccessShow">
                  <label class="control-label labelForm col-sm-2" for="estilo_show">Estilo:</label>
                  <div class="col-sm-10">
                  <select class="form-control" id="estilo_show" name="estilo_show" style="max-width: 200px">
                    <option value="1">AXÉ</option>
                    <option value="2">BOSSA NOVA</option>
                    <option value="3">FORRÓ</option>
                    <option value="4">FUNK</option>
                    <option value="5">GOLSPEL</option>
                    <option value="6">HIP HOP</option>
                    <option value="7">JAZZ</option>
                    <option value="8">MPB</option>
                    <option value="9">PAGODE</option>
                    <option value="10">POP</option>
                    <option value="11">RAP</option>
                    <option value="12">REGGAE</option>
                    <option value="13">ROCK</option>
                    <option value="14">SAMBA</option>
                    <option value="15">SERTANEJO</option>
                    <option value="16">Outros</option>
                  </select>
                  </div>
                </div>

                <!--File-->
                <div class="form-group addSuccessShow">
                  <label class="control-label labelForm col-sm-2" for="inputImagem_show">Imagem:</label>
                    <input type="file" class="form-control-file" id="inputImagem_show" name="inputImagem_show" aria-describedby="inputShowTexto">
                    <small id="inputShowTexto" class="form-text text-muted">Apenas imagens com tamanho máximo de 1Mb serão permitidas.</small>
                </div>

                
                <div class="form-group addSuccessShow"> 
                  <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-default">Cadastrar</button>
                      <img  id="loadCadastroShow" src="../imagens/gif/load1.gif" style="max-width: 50px;">
                  </div>
                </div>

              <p class="text-center animated bounceInRight" id="sucessoCadastroShow"><span class="glyphicon glyphicon-ok"></span><strong> Show cadastrado com sucesso, iremos examiná-lo para publicá-lo.</strong></p>
                <p class="text-center" id="erroCadastroShow"></p><br>
                </fieldset>
            </form>


          </div>
        </div>
      </div>

      <div class="panel panel-default panel-festivais">
        <div class="panel-heading titulofestivais" data-toggle="collapse" data-target="#collapse2" data-parent="#accordion" onclick="mudarIconeEventos('#collapse2','.setaAccordion2')">
          <h3 class="panel-title text-center">Festivais
            <p class="textoAccordion setaAccordion2"><span class="glyphicon glyphicon-menu-down iconeAccordion"></span></p>
          </h3>
        </div>
        <div id="collapse2" class="panel-collapse coll-exame collapse">
          <div class="panel-body">


            <form class="form-horizontal" id="formCadastroFest">
              <fieldset id="fieldsetCadastroFest">
                <!--Nome-->
              <div class="form-group addSuccessFest">
                  <label class="control-label col-sm-2" for="nome_fest">Nome:</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nome_fest" name="nome_fest" placeholder="Informe o nome do Festival" required>
                  </div>
              </div>

                <!--Data-->
                <div class="form-group addSuccessFest">
                  <label class="control-label col-sm-2" for="date_fest">Data:</label>
                  <div class="col-sm-10">
                  <input class="form-control" type="datetime-local" id="date_fest" name="date_fest" style='max-width: 220px;' required>
                  Informe a data e a hora do Festival.
                  </div>
                </div>

                <!--Endereço-->
                <div class="form-group addSuccessFest">
                  <label class="control-label labelForm col-sm-2" for="local_fest">Local:</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="local_fest" name="local_fest" placeholder="Informe o endereço do Festival" required></textarea>
                  </div>
                </div>

                <!--Telefone-->
                <div class="form-group addSuccessFest">
                  <label class="control-label col-sm-2" for="telefone_fest">Telefone:</label>
                  <div class="col-sm-10">
                    <input pattern="^\d{2}-\d{5}-\d{4}$" type="tel" class="form-control" rows="3" id="telefone_fest" name="telefone_fest" OnKeyPress="formatar('##-#####-####', this)" maxlength="13" placeholder="00-00000-0000" style="max-width: 200px;" required></input>
                    Informe um telefone para contato.<br>Caso deseje informar um Nº Fixo, colocar um 0 no lugar do 9º dígito.
                  </div>
                </div>

                  <!--Preços-->
                <div class="form-group addSuccessFest">
                  <label class="control-label labelForm col-sm-2" for="precos_fest">Preços:</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="precos_fest" name="precos_fest" placeholder="Descreva informações sobre preços do Festival" required></textarea>
                  </div>
                </div>

                <div class="form-group addSuccessFest">
                  <label class="control-label labelForm col-sm-2" for="estilo_fest">Estilo:</label>
                  <div class="col-sm-10">
                  <select class="form-control" id="estilo_fest" name="estilo_fest" style="max-width: 200px">
                    <option value="1">DANÇA</option>
                    <option value="2">MÚSICA</option>
                    <option value="3">TEATRO</option>
                    <option value="4">OUTROS</option>
                  </select>
                  </div>
                </div>

                <!--File-->
                <div class="form-group addSuccessFest">
                  <label class="control-label labelForm col-sm-2" for="inputImagem_fest">Imagem:</label>
                    <input type="file" class="form-control-file" id="inputImagem_fest" name="inputImagem_fest" aria-describedby="inputFestTexto">
                    <small id="inputFestTexto" class="form-text text-muted">Apenas imagens com tamanho máximo de 1Mb serão permitidas.</small>
                </div>

                
                <div class="form-group addSuccessFest"> 
                  <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-default">Cadastrar</button>
                      <img  id="loadCadastroFest" src="../imagens/gif/load1.gif" style="max-width: 50px;">
                  </div>
                </div>

              <p class="text-center animated bounceInRight" id="sucessoCadastroFest"><span class="glyphicon glyphicon-ok"></span><strong> Festival cadastrado com sucesso, iremos examiná-lo para publicá-lo.</strong></p>
                <p class="text-center" id="erroCadastroFest"></p><br>
                </fieldset>
            </form>

          
          </div>
        </div>
      </div>



</div>

<?php require_once ("footer.php"); ?>
<script async src="js/main.js"></script>

</body>
</html>
