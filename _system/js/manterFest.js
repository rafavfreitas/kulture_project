//Manter dados do cliente.
$(document).ready(function () {
   $(".alterarData").click(function () {
      $('#dateAt').attr("disabled", $(this).is(":checked"));
  });
});

$(document).ready(function () {
   $(".alterarImg").click(function () {
      $('#inputImagem_At').attr("disabled", $(this).is(":checked"));
   });
});


$(document).ready(function(){

    // Função Clicar Botão Ver
    $('button#ver').click(function() {
        var idVer=$(this).val();
        $.ajax({
            url:"../db/manterFest.php",                    
            type:"post",                            
            data: "Fest_Id_At="+idVer,
            dataType: "JSON",
            success: function (result){ 
                //var dados = JSON.parse(result);
                $("#nomeVer").html("<dd id='nomeVer'>"+result[0].fest_nome+"</dd>");
                $("#dataVer").html("<dd id='nomeVer'>"+result[0].fest_data+"</dd>");
                $("#localVer").html("<dd id='nomeVer'>"+result[0].fest_local+"</dd>");
                $("#contatoVer").html("<dd id='nomeVer'>"+result[0].fest_contato+"</dd>");
                $("#precoVer").html("<dd id='nomeVer'>"+result[0].fest_preco+"</dd>");
                $("#publicadoVer").html("<dd id='nomeVer'>"+result[0].fest_publicado+"</dd>");
                if (result[0].fest_liberado == 0) {
                    $("#liberadoVer").html("<dd id='nomeVer'>Sendo avaliado.</dd>");
                }else{
                    $("#liberadoVer").html("<dd id='nomeVer'>Publicação autorizada</dd>");
                }

                $("#myModalVer").modal({backdrop: false});
            }
        });

    });// Fim da Função Clicar Botão Ver




            // Função Clicar Botão Atualizar
            $('button#atualizar').click(function() {
                $('#loadAtualizarDados').hide();
                $('#retornoAt').hide();
                var idAtualizar=$(this).val();
                $.ajax({
                    url:"../db/manterFest.php",                    
                    type:"post",                            
                    data: "Fest_Id_At="+idAtualizar,
                    dataType: "JSON",
                    success: function (result){ 
                        //var dados = JSON.parse(result);
                        $("#idAt").val(result[0].fest_id);
                        $("#nomeAt").val(result[0].fest_nome);
                        $("#dataAtual").html("<p style='margin: 0px;' >"+result[0].fest_data+"</p>");
                        $("#enderecoAt").val(result[0].fest_local);
                        $("#contatoAt").val(result[0].fest_contato);
                        $("#precoAt").val(result[0].fest_preco);
                        $('#estiloAt option[value='+result[0].estilo_fest_id+']').attr('selected','selected');
                        $("#myModalAtualizar").modal({backdrop: false});
                    }
                });

            });// Fim da Função Clicar Botão Atualizar

            $('#sucessoAtualizarShow').hide();
            $('#formAtualizar').submit(function(){  //Ao submeter formulário
                $('#loadAtualizarDados').show();
                $('#sucessoAtualizarShow').hide();
                $('#erroAtualizarShow').hide();
                var formData = new FormData($(this)[0]);
                $.ajax({            //Função AJAX
                    url:"../db/manterFest.php",
                    type: 'POST',
                    data: formData,
                    async: false,                    //Arquivo php
                    //data: "nome_show="+nome_show+"&date_show="+date_show+"&local_show="+local_show+"&telefone_show="+telefone_show+"&precos_show="+precos_show+"&estilo_show="+estilo_show+"&inputImagem_show="+inputImagem_show,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==1){
                            $('#loadAtualizarDados').hide();
                            $('#fieldsetAtualizarShow').attr("disabled", "disabled");
                            $(".addSuccessShow").addClass("has-success has-feedback");
                            $('#sucessoAtualizarShow').show();
                            $('#sucessoAtualizarShow').addClass('animated shake'); 
                            $(document).ready(function(){
                                $("button.close").click(function(){
                                    location.reload();
                                });
                            });                  
                        }if (result !=1 ){
                            $('#loadAtualizarDados').hide();
                            $("#erroAtualizarShow").html("<p class='text-center'> "+ result+ "</p>");
                            $('#erroAtualizarShow').show();
                            $('#erroAtualizarShow').addClass('animated shake');
                            $(document).ready(function(){
                                $("button.close").click(function(){
                                    location.reload();
                                });
                            });
                        }  
                    },
                    
                    cache: false,
                    contentType: false,
                    processData: false
                });
            return false;   //Evita que a página seja atualizada
        });


            // Função Submit Modal Atualizar Desativada pois estava sendo enviado por JSON
            /*
            $('#formAtualizar').submit(function(){
                $('#loadAtualizarDados').show();
                $('#retornoAt').hide();
                var json = jQuery(this).serialize();
                $.ajax({
                    type: "POST",
                    url: "../db/manterFest.php",
                    data: json,
                    success: function(result)
                    {
                        if(result==1){
                            $('#loadAtualizarDados').hide();
                            $('#retornoAt').show();
                            $('#retornoAt').addClass('animated shake');                     
                            $("#retornoAt").html("<p class='text-center'>As informações foram atualizadas e seguirão para análise.</p>");
                            $(document).ready(function(){
                                $("button.close").click(function(){
                                    location.reload();
                                });
                            });

                        }if (result !=1){
                            $('#retornoAt').show();
                            $('#retornoAt').addClass('animated shake');                     
                            $("#retornoAt").html("<p class='text-center'>Ops!: "+ result+ "</p>");
                        }
                    }
                });

                return false;
            });// Fim da Função Submit Modal Atualizar */
            
            // Função Clicar Botão Deletar
    $('button#apagar').click(function() {
            var r = confirm("Você confirma a exclusão do Show? Caso sim, não poderá ser recuperado!");
            if (r == true) {
                var idDeletar=$(this).val();
                $.ajax({
                    url:"../db/manterFest.php",                    
                    type:"post",                            
                    data: "deletar_fest_id="+idDeletar,
                            //dataType: "JSON",
                            success: function (result){ 
                                if (result == 1) {
                                    alert("Show excluído!");
                                    location.reload();
                                }if (result !=1){
                                    alert("Ops!: "+ result+ ".");                    
                                }


                            }
                        });
                }
            });// Fim da Função Clicar Botão Deletar
            

});