//Função AJAX para Cadastrar Show
$(document).ready(function(){
        $('#loadCadastroShow').hide();
        $('#sucessoCadastroShow').hide();
        $('#erroCadastroShow').hide();
            $('#formCadastroShow').submit(function(){  //Ao submeter formulário
                $('#loadCadastroShow').show();
                $('#sucessoCadastroShow').hide();
                $('#erroCadastroShow').hide();
                var formData = new FormData($(this)[0]);
                $.ajax({            //Função AJAX
                    url:"../db/cadastrarShow.php",
                    type: 'POST',
                    data: formData,
                    async: false,                    //Arquivo php
                    //data: "nome_show="+nome_show+"&date_show="+date_show+"&local_show="+local_show+"&telefone_show="+telefone_show+"&precos_show="+precos_show+"&estilo_show="+estilo_show+"&inputImagem_show="+inputImagem_show,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==1){
                            $('#loadCadastroShow').hide();
                            $('#fieldsetCadastroShow').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".addSuccessShow").addClass("has-success has-feedback");
                            $('#sucessoCadastroShow').show();
                            $('#sucessoCadastroShow').addClass('animated shake');                   
                        }if (result !=1 ){
                            $('#loadCadastroShow').hide();
                            $("#erroCadastroShow").html("<p class='text-center'>Desculpe: "+ result+ "</p>");
                            $('#erroCadastroShow').show();
                            $('#erroCadastroShow').addClass('animated shake');
                        }  
                    },
                    
                    cache: false,
                    contentType: false,
                    processData: false
                });
            return false;   //Evita que a página seja atualizada
            });
});


//Função AJAX para Cadastrar Festival
$(document).ready(function(){
        $('#loadCadastroFest').hide();
        $('#sucessoCadastroFest').hide();
        $('#erroCadastroFest').hide();
            $('#formCadastroFest').submit(function(){  //Ao submeter formulário
                $('#loadCadastroFest').show();
                $('#sucessoCadastroFest').hide();
                $('#erroCadastroFest').hide();
                var formData = new FormData($(this)[0]);
                $.ajax({            //Função AJAX
                    url:"../db/cadastrarFest.php",
                    type: 'POST',
                    data: formData,
                    async: false,                    //Arquivo php
                    //data: "nome_show="+nome_show+"&date_show="+date_show+"&local_show="+local_show+"&telefone_show="+telefone_show+"&precos_show="+precos_show+"&estilo_show="+estilo_show+"&inputImagem_show="+inputImagem_show,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==1){
                            $('#loadCadastroFest').hide();
                            $('#fieldsetCadastroFest').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".addSuccessFest").addClass("has-success has-feedback");
                            $('#sucessoCadastroFest').show();
                            $('#sucessoCadastroFest').addClass('animated shake');                   
                        }if (result !=1 ){
                            $('#loadCadastroFest').hide();
                            $("#erroCadastroFest").html("<p class='text-center'>Desculpe: "+ result+ "</p>");
                            $('#erroCadastroFest').show();
                            $('#erroCadastroFest').addClass('animated shake');
                        }  
                    },
                    
                    cache: false,
                    contentType: false,
                    processData: false
                });
            return false;   //Evita que a página seja atualizada
            });
});


$(document).ready(function(){
        $('#loadAtualizarUser').hide();
        $('#sucessoAtualizarUser').hide();
        $('#erroAtualizarUser').hide();
            $('#formAtualizarUser').submit(function(){  //Ao submeter formulário
                $('#loadAtualizarUser').show();
                $('#sucessoAtualizarUser').hide();
                $('#erroAtualizarUser').hide();
                var formData = new FormData($(this)[0]);
                $.ajax({            //Função AJAX
                    url:"../db/manterUser.php",
                    type: 'POST',
                    data: formData,
                    async: false,                    //Arquivo php
                    //data: "nome_show="+nome_show+"&date_show="+date_show+"&local_show="+local_show+"&telefone_show="+telefone_show+"&precos_show="+precos_show+"&estilo_show="+estilo_show+"&inputImagem_show="+inputImagem_show,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==1){
                            $('#loadAtualizarUser').hide();
                            $('#fieldsetAtualizarUser').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".addSuccessFest").addClass("has-success has-feedback");
                            $('#sucessoAtualizarUser').show();
                            $('#sucessoAtualizarUser').addClass('animated shake');                   
                        }if (result !=1 ){
                            $('#loadAtualizarUser').hide();
                            $("#erroAtualizarUser").html("<p class='text-center'>Desculpe: "+ result+ "</p>");
                            $('#erroAtualizarUser').show();
                            $('#erroAtualizarUser').addClass('animated shake');
                        }  
                    },
                    
                    cache: false,
                    contentType: false,
                    processData: false
                });
            return false;   //Evita que a página seja atualizada
            });
});





$(document).ready(function(){
  // Código para adicionar suavidade no deslizamento da página através do menu.
  $(".navbar a, footer a[href='#myPage'], .btnEntraEmContato").on('click', function(event) {
    //Certifique-se de que this.hash tem um valor antes de substituir o comportamento padrão
    if (this.hash !== "") {
      // Prevenir o comportamento de clique de âncora padrão
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Usando o método jQuery animate () para adicionar scroll de página suave
      // Um número opcional (900) que significa o número de milissegundos necessários para rolar para a área escolhida.
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Adicionar hash(#) ao URL quando feito scroll (comportamento de clique padrão)
        window.location.hash = hash;
      });
    } // Fim do IF
  });
});

  function mudarIconeEventos(div,icone){
    $(div).on("hidden.bs.collapse", function(){
      $(icone).html('<span class="glyphicon glyphicon-menu-down iconeAccordion"></span>');
    });
  
    $(div).on("show.bs.collapse", function(){
      $('.textoAccordion').html('<span class="glyphicon glyphicon-menu-down iconeAccordion"></span>');
      $(icone).html('<span class="glyphicon glyphicon-menu-up iconeAccordion"></span>');
      $(icone).html('<span class="glyphicon glyphicon-menu-up iconeAccordion"></span>');
    });
}

function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)
  
  if (texto.substring(0,1) != saida){
     documento.value += texto.substring(0,1);
  }
  
}