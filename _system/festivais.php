<?php
include '../db/consultaFest.php';
?>
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Kuture</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="../imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">
  <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <?php require_once ("validaSessao.php"); ?>
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

  <nav class="navbar navbarIndex navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="../imagens/logos/logo4.2.png"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navIndex navbar-right">
          <li><a href="index.php">HOME</a></li>
          <li><a href="shows.php">MEUS SHOWS</a></li>
          <li class="active"><a href="festivais.php">MEUS FESTIVAIS</a></li>
          <li><a href="conta.php">MINHA CONTA</a></li>
          <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>Sair</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="jumbotron jumbIndex text-center">
    <img id="logo_maior" class="animated fadeIn" src="../imagens/logos/logo1.1.png">
    <p id="textoLogo" class="animated fadeInUp">Amamos a música</p> 

  <!--form>
    <div class="input-group">
      <input type="email" class="form-control" size="50" placeholder="Email Address" required>
      <div class="input-group-btn">
        <button type="button" class="btn btn-danger"></button>
      </div>
    </div>
  </form-->
</div>


<div class="modal fade" id="myModalVer" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="max-width: 300px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4><span class="glyphicon glyphicon-user"></span> Informações do Festival</h4>
      </div>
      <div class="modal-body" style="padding:30px 40px;">
        <dl>
          <dt>Nome:</dt>
          <dd id="nomeVer"></dd>
          <dt>Data Show:</dt>
          <dd id="dataVer"></dd>
          <dt>Local:</dt>
          <dd id="localVer"></dd>
          <dt>Contato:</dt>
          <dd id="contatoVer"></dd>
          <dt>Preço:</dt>
          <dd id="precoVer"></dd>
          <dt>Publicado na ferramenta:</dt>
          <dd id="publicadoVer"></dd>
          <dt>Situação:</dt>
          <dd id="liberadoVer"></dd>
        </dl> 
      </div> <!--modal-body (Ver Dados do Show)-->
    </div><!--modal-content (Ver Dados do Show)-->
  </div><!--modal-dialog (Ver Dados do Show)-->
</div> <!--modal fade (Ver Dados do Show)-->


<div class="modal fade" id="myModalAtualizar" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3><span class="glyphicon glyphicon-pencil"></span> Atualizar Dados</h3>
      </div>
      <div class="modal-body" style="padding:30px 40px;">

        <form class="form-horizontal" id="formAtualizar" style="max-width: 600px;">
          <fieldset id="fieldsetAtualizarShow">
            <!--Nome-->
            <div class="form-group">
              <label class="control-label col-sm-2" for="nomeAt">Nome:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="nomeAt" name="nomeAt" placeholder="Informe o nome do usuário" required>
              </div>
            </div>

            <!--Data-->
            <div class="form-group">
              <label class="control-label col-sm-2" for="dateAt">Data:</label>
              <div class="col-sm-10">
                <p style="margin: 0px;" class="dataAtual" id="dataAtual">Teste</p>
                <input type="checkbox" class="alterarData" id="alterarData" name="alterarData" value="alterarData"> Manter data atual
                <input class="form-control novaData" type="datetime-local" id="dateAt" name="dateAt" style='max-width: 210px;' required>
              </div>
            </div>

            <!--Endereço -->
            <div class="form-group">
              <label class="control-label col-sm-2" for="enderecoAt">Endereço:</label>
              <div class="col-sm-10"> 
                <textarea class="form-control" rows="3" id="enderecoAt" name="enderecoAt" style="resize:vertical;" required></textarea>
              </div>
            </div>

            <!--Telefone-->
            <div class="form-group">
              <label class="control-label col-sm-2" for="contatoAt">Contato:</label>
              <div class="col-sm-10">
                <input pattern="^\d{2}-\d{5}-\d{4}$" type="tel" class="form-control" rows="3" id="contatoAt" name="contatoAt" OnKeyPress="formatar('##-#####-####', this)" maxlength="13" placeholder="00-00000-0000" style="max-width: 150px;" required></input>Caso deseje informar um Nº Fixo, colocar um 0 no lugar do 9º dígito.
              </div>
            </div>

            <!--Precos-->
            <div class="form-group">
              <label class="control-label col-sm-2" for="precoAt">Preços:</label>
              <div class="col-sm-10"> 
                <textarea class="form-control" rows="3" id="precoAt" name="precoAt" style="resize:vertical;" required></textarea>
                <input type="hidden" name="idAt" id="idAt" value="">
              </div>
            </div>

            <!--Estilo-->
            <div class="form-group">
              <label class="control-label labelForm col-sm-2" for="estiloAt">Estilo:</label>
              <div class="col-sm-10">
                <select class="form-control" id="estiloAt" name="estiloAt" style="max-width: 200px">
                  <option value="1">DANÇA</option>
                  <option value="2">MÚSICA</option>
                  <option value="3">TEATRO</option>
                  <option value="4">OUTROS</option>
                </select>
              </div>
            </div>

            <!--Nova Imagem-->
            <div class="form-group">
              <label class="control-label labelForm col-sm-2" for="inputImagem_At">Imagem:</label>
              <input type="checkbox" class="alterarImg" name="alterarImg" value="alterarImg"> Manter Imagem atual
              <input type="file" class="form-control-file" id="inputImagem_At" name="inputImagem_At" aria-describedby="inputShowTexto">
              <small id="inputShowTexto" class="form-text text-muted">Apenas imagens com tamanho máximo de 1Mb serão permitidas.</small>
            </div>

            <div class="form-group"> 
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default" id="btnAtualizar">Atualizar</button>
                <div id="divGif"><img  id="loadAtualizarDados" src="../imagens/gif/load.gif" style="max-width: 50px;"></div>
                <p id="sucessoAtualizarShow" class="text-center">Dados atualizados, antes de serem publicados, serão avaliados.</p>
                <p id="erroAtualizarShow" class="text-center"></p>
              </div>
            </div>

          </fieldset>

        </form>
      </div> <!--modal-body (Atualizar)-->
    </div><!--modal-content (Atualizar)-->
  </div><!--modal-dialog (Atualizar)-->
</div> <!--modal fade (Atualizar)-->



<!-- Container (sobre Section) -->
<div id="eventos" class="container-fluid">

  <div class="container">

    <div class="row" style="margin-top: 25px;">
      <div class="col-sm-3">
        <p>
          <h3>Festivais</h3>
        </p>
      </div>
      <div class="col-sm-6">
        <form class="form-horizontal" method="GET" action="festivais.php" style="max-width:500px; margin-bottom: 20px;">
          <div class="input-group col-md-12">
            <?php 
            include '../db/datalistFestNome.php';
            ?>
            <span class="input-group-btn">
              <button class="btn btn-info btn-md" type="submit">
                <i class="glyphicon glyphicon-search"></i>
              </button>
            </span>
          </div>
        </form>
      </div> 


      <table class="table table-striped table-bordered" id="tableUser">
        <thead>
          <tr>
            <th>Nome</th>
            <th id="ocultarTitulo1">Data</th>
            <th id="ocultarTitulo2">Estilo</th>
            <th>Ações</th>
          </tr>
        </thead>
        <?php 
        try {
          foreach ($pdo->query($sql) as $row) {
            echo '<tr>';
            echo '<td>'. $row['fest_nome'] . '</td>';
            echo '<td class="ocultarDataEstilo">'. $row['fest_data'] . '</td>';
            echo '<td class="ocultarDataEstilo ocultarEstilo">'. $row['estilo_fest_nome'] . '</td>';

            echo '<td width=250>';
            echo '<button type="button" class="btn" id="ver" value="'.$row['fest_id'].'">Ver</button>';
            echo '&nbsp;';
            echo '<button type="button" class="btn btn-success" id="atualizar" value="'.$row['fest_id'].'">Atualizar</button>';
            echo '&nbsp;';
            echo '<button type="button" class="btn btn-danger" id="apagar" value="'.$row['fest_id'].'">Apagar</button>';
                            //echo '<a class="btn btn-danger" href="delete.php?id='.$row['id_User'].'">Apagar</a> onclick="deleteUser('.$row['fest_id'].');"';
            echo '</td>';
            echo '</tr>';
          }
        } catch (Exception $e) {
          echo 'ERROR: ' . $e->getCode() .'Formação das Linhas';
        }?>

      </table>
    </div><!--Class Row-->
    <?php
                            //Verificar a pagina anterior e posterior
    $pagina_anterior = $pagina - 1;
    $pagina_posterior = $pagina + 1;
    ?>
                        <?php //Se estiver detro de uma pesquisa gera essa paginação
                        if (!empty($_GET['enter'])){?>
                        <nav class="text-center">
                          <ul class="pagination">
                            <li>
                              <?php
                              if($pagina_anterior != 0){ ?>
                              <a href="festivais.php?enter=<?php echo $pesquisa;?>&pagina=<?php echo $pagina_anterior; ?>" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                              </a>
                              <?php }else{ ?>
                              <span aria-hidden="true">&laquo;</span>
                              <?php }  ?>
                            </li>
                            <?php 
                                    //Apresentar a paginacao
                            for($i = 1; $i < $num_pagina + 1; $i++){
                              if ($i == $pagina){
                                ?>
                                <li class="active"><a href="festivais.php?enter=<?php echo $pesquisa;?>&pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                <?php }else{?>
                                <li><a href="festivais.php?enter=<?php echo $pesquisa;?>&pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                <?php } } ?>
                                <li>
                                  <?php
                                  if($pagina_posterior <= $num_pagina){ ?>
                                  <a href="festivais.php?enter=<?php echo $pesquisa;?>&pagina=<?php echo $pagina_posterior; ?>" aria-label="Previous">
                                    <span aria-hidden="true">&raquo;</span>
                                  </a>
                                  <?php }else{ ?>
                                  <span aria-hidden="true">&raquo;</span>
                                  <?php }  ?>
                                </li>
                              </ul>
                            </nav>



                            <?php }else{ //Caso não esteja dentro de uma pesquisa.
                              ?>
                              <nav class="text-center">
                                <ul class="pagination">
                                  <li>
                                    <?php
                                    if($pagina_anterior != 0){ ?>
                                    <a href="festivais.php?pagina=<?php echo $pagina_anterior; ?>" aria-label="Previous">
                                      <span aria-hidden="true">&laquo;</span>
                                    </a>
                                    <?php }else{ ?>
                                    <span aria-hidden="true">&laquo;</span>
                                    <?php }  ?>
                                  </li>
                                  <?php 
                                    //Apresentar a paginacao
                                  for($i = 1; $i < $num_pagina + 1; $i++){ 
                                    if ($i == $pagina) { ?>
                                    <li class="active"><a href="festivais.php?pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                    <?php }else{ ?>
                                    <li><a href="festivais.php?pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                    <?php } }?>
                                    <li>
                                      <?php
                                      if($pagina_posterior <= $num_pagina){ ?>
                                      <a href="festivais.php?pagina=<?php echo $pagina_posterior; ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                      </a>
                                      <?php }else{ ?>
                                      <span aria-hidden="true">&raquo;</span>
                                      <?php }  ?>
                                    </li>
                                  </ul>
                                </nav><?php } ?>
                              </div> <!-- /container -->



                            </div>

                            <?php require_once ("footer.php"); ?>
                            
<script async src="js/main.js"></script>
<script async src="js/manterFest.js"></script>

</body>
</html>
