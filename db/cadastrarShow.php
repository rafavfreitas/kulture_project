<?php
require_once ("validaSessao.php");

$user_id = $_SESSION['UsuarioID'];

if (!empty($_FILES['inputImagem_show'])) {
	//ini_set("upload_max_filesize", "5M");
	
	
	$dir = '../imagens/img_eventos/shows/'; //Diretório para uploads
	$target_file = $dir . basename($_FILES["inputImagem_show"]["name"]);
	$extensao = pathinfo($target_file,PATHINFO_EXTENSION);
	$ext = "." . $extensao;
	//$ext = strtolower(substr($_FILES['inputImagem_show']['name'],-4)); //Pegando extensão do arquivo
    //$ext = '.jpg'; //Definir a extensão do arquivo
    //$new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo

	$erroTamanho = "";
	$erroExtensao = "";
											  
    if ($_FILES["inputImagem_show"]["size"] > 1024000) {
    	//upload_max_filesize
		$erroTamanho = "Arquivo acima do limite, favor escolher um arquivo menor";
	}

	if($extensao != "jpg" && $extensao != "png" && $extensao != "jpeg" ) {
    	$erroExtensao = "Somente serão aceitos arquivos JPG, JPEG & PNG.";
  	}
  	$ext = ".jpg";//Definir a extensão que o arquivo será salvo na base, o PHP irá converter lá em baixo.

    
}if (empty($_POST) AND (empty($_POST['nome_show']) OR empty($_POST['date_show']) OR empty($_POST['local_show']) OR empty($_POST['telefone_show']) OR empty($_POST['precos_show'])  OR empty($_POST['estilo_show']) OR empty($_FILES['inputImagem_show']) )) {
    header("Location: ../index.php"); exit;

}elseif (!empty($erroTamanho) || !empty($erroExtensao)) {
	echo "$erroTamanho";
	echo "<br>";
	echo "$erroExtensao";

}else{

	$nome_show = $_POST["nome_show"];
	$date_show = $_POST['date_show'];
	$local_show = $_POST["local_show"];
	$telefone_show = $_POST["telefone_show"];
	$precos_show = $_POST["precos_show"];
	$estilo_show = $_POST["estilo_show"];
	$show_liberado = 0;

	try {
		include 'conexao/conecta.php';

		$sql = $pdo->prepare("insert into shows (show_nome, show_data, show_local, show_contato, show_preco, show_publicado, show_liberado, estilo_show_id, user_id) values (?,?,?,?,?,NOW(),?,?,?);");

		$sql->bindParam(1, $nome_show , PDO::PARAM_STR);
		$sql->bindParam(2, $date_show , PDO::PARAM_STR);
		$sql->bindParam(3, $local_show , PDO::PARAM_STR);
		$sql->bindParam(4, $telefone_show , PDO::PARAM_STR);
		$sql->bindParam(5, $precos_show , PDO::PARAM_STR);
		$sql->bindParam(6, $show_liberado , PDO::PARAM_INT);
		$sql->bindParam(7, $estilo_show , PDO::PARAM_INT);
		$sql->bindParam(8, $user_id , PDO::PARAM_INT);
		$sql->execute();

		$idFotoShow = $pdo->lastInsertId(); 
		//Pegar ID da tabela para atribuir para imagem respectiva
		$new_name = $idFotoShow . $ext; 
		//Código para mover a foto
		move_uploaded_file($_FILES['inputImagem_show']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
		
		echo "1";

	} catch (Exception $erro01) {
		echo "Erro: $erro01";
		echo "Erro PDO: Não foi possivel Cadastrá-lo na base de dados, tente novamente.";
	}
	
  
}//Fim 1º ELSE




?>