<?php
if (empty($_POST) AND (empty($_POST['nome']) OR empty($_POST['email']) OR empty($_POST['telefone']) OR empty($_POST['datenasc']) OR empty($_POST['login']) OR empty($_POST['senha']) )) {
    header("Location: ../index.php"); exit;
}else{

	$nome = $_POST["nome"];
	$email = $_POST['email'];
	$telefone = $_POST["telefone"];
	$data = $_POST["datenasc"];
	$login = $_POST["login"];
	$senha = $_POST["senha"];
	$nivel = "0";
	try {
		include 'conexao/conecta.php';

		$sql = $pdo->prepare("select * from usuarios where user_email = ?");
		$sql->bindParam(1, $email , PDO::PARAM_STR);
		$sql->execute();
		$count1 = $sql->rowCount();

		$sql = $pdo->prepare("select * from usuarios where user_login = ?");
		$sql->bindParam(1, $login , PDO::PARAM_STR);
		$sql->execute();
		$count2 = $sql->rowCount();

		if ($count1 == 1) {
			echo "Email já cadastrado! Informe outro.";
		}elseif($count2 == 1){
			echo "Login já cadastrado! Informe outro.";
		}else{

			$sql = $pdo->prepare("insert into usuarios (user_nome, user_email, user_telefone, user_datanasc, user_login, user_senha, user_nivel) values (?,?,?,?,?,sha1(?),?)");

			$sql->bindParam(1, $nome , PDO::PARAM_STR);
			$sql->bindParam(2, $email , PDO::PARAM_STR);
			$sql->bindParam(3, $telefone , PDO::PARAM_STR);
			$sql->bindParam(4, $data , PDO::PARAM_STR);
			$sql->bindParam(5, $login , PDO::PARAM_STR);
			$sql->bindParam(6, $senha , PDO::PARAM_STR);
			$sql->bindParam(7, $nivel , PDO::PARAM_STR);
			$sql->execute();
			$idUser = $pdo->lastInsertId(); 
			$count = $sql->rowCount();


			if ($count == 1) {
				$sql = $pdo->prepare("CALL criarEstilos(".$idUser.");");
				$sql->execute();

		 		require_once('class/class.phpmailer.php'); 

			    try {
			 		
					$mail = new PHPMailer(true);// Inicia a classe PHPMailer
					$mail->CharSet = 'UTF-8';
					$mail->IsSMTP(); // Define que a mensagem será SMTP
					$mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
					$mail->SMTPSecure = 'tls';
					$mail->Host = 'smtp.gmail.com'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
			     	$mail->Port       = 587; //  Usar 587 porta SMTP
			     	$mail->Username = 'projetokuture@gmail.com'; // Usuário do servidor SMTP (endereço de email)
			     	$mail->Password = 'kuture123'; // Senha do servidor SMTP (senha do email usado)
			 		
			 		$mail->SetFrom('projetokuture@gmail.com' , 'Sistema Kuture'); //Seu e-mail
			     	$primeiroNome = explode(" ", $nome); //Pegar apenas o primeiro nome
				    $mail->Subject  =   "Bem vindo ao Kuture";
				    $mail->AddAddress("$email");
					$body = file_get_contents('emails/email_cadastro.html');
					$body = str_replace('%primeiroNome%', $primeiroNome[0], $body); //Substituir variável dentro do HTML
					$mail->MsgHTML($body);
					$mail->IsHTML(true); // send as HTML
				    $enviarParaUsuario = $mail->Send();//Enviar E-Mail

					if ($enviarParaUsuario) {
					      echo '1';
					} else {
					       echo '2';
					}

				}catch (phpmailerException $e) {
			    	//echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
			    	echo "Você foi cadastrado, mas não conseguimos te enviar um e-mail de confirmação.";
				}


			}else {
	    		echo "Erro de Base: Não foi possivel Cadastrá-lo na base de dados, tente novamente.";//Erro de inserção no banco.
	  		}




		}//Fim do else que consultava a duplicidade

		


		
	} catch (Exception $erro01) {
		//echo "Erro: $erro01";
		echo "Erro PDO: Não foi possivel Cadastrá-lo na base de dados, tente novamente.";
	}
	
  
}//Fim 1º ELSE




?>