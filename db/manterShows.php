<?php

//Pegar Dados dos Clientes
if (!empty($_POST['Show_Id_At'])) {
  try{
    $Show_Id_At = $_POST["Show_Id_At"];
    include 'conexao/conecta.php';

    $sql = $pdo->prepare("select s.show_id, s.show_nome, DATE_FORMAT(show_data,'%d/%m/%Y às %H:%i:%s') AS show_data, s.show_local, s.show_contato, s.show_preco, s.show_publicado, s.show_liberado, s.estilo_show_id, es.estilo_show_nome from shows s inner join estilo_show es on s.estilo_show_id = es.estilo_show_id WHERE s.show_id = ? LIMIT 1");
    $sql->bindParam(1, $Show_Id_At , PDO::PARAM_INT);
    $sql->execute();

    $result=$sql->fetchAll(PDO::FETCH_ASSOC);//FETCH_ASSOC
    //$output[] = $result;

    $json=json_encode($result);
    echo "$json";    

    
  }
  catch(PDOException $e){
    echo $e->getCode();
  }

//Deletar Clientes
}elseif (!empty($_POST['deletar_show_id'])) {
	try{
    $show_id = $_POST["deletar_show_id"];
    include 'conexao/conecta.php';

    $sql = $pdo->prepare("delete FROM shows WHERE show_id = ?");
    $sql->bindParam(1, $show_id , PDO::PARAM_INT);
    $sql->execute();

    $count = $sql->rowCount();

    if ($count == 1) {
    	echo "1";
    }else{
    	echo "Ocorreu um ERRO na execução da instrução!";
    }   

  }
  catch(PDOException $e){
    echo $e->getCode();
  }	


//Atualizar dados dos Clientes
}elseif (!empty($_POST)) {


  if (!empty($_FILES['inputImagem_At'])) {
  //ini_set("upload_max_filesize", "5M");


  $dir = '../imagens/img_eventos/shows/'; //Diretório para uploads
  $target_file = $dir . basename($_FILES["inputImagem_At"]["name"]);
  $extensao = pathinfo($target_file,PATHINFO_EXTENSION);
  $ext = "." . $extensao;
 

  $erroTamanho = "";
  $erroExtensao = "";

  if ($_FILES["inputImagem_At"]["size"] > 1024000) {
      //upload_max_filesize
    $erroTamanho = "Arquivo acima do limite, favor escolher um arquivo menor";
  }

  if($extensao != "jpg" && $extensao != "png" && $extensao != "jpeg" ) {
    $erroExtensao = "Somente serão aceitos arquivos JPG, JPEG & PNG.";
  }
  $ext = ".jpg";//Definir a extensão que o arquivo será salvo na base, o PHP irá converter lá em baixo.

}if (empty($_POST) AND (empty($_POST['nome_show']) OR empty($_POST['date_show']) OR empty($_POST['local_show']) OR empty($_POST['telefone_show']) OR empty($_POST['precos_show'])  OR empty($_POST['estilo_show']) OR empty($_FILES['inputImagem_show']) )) {
    header("Location: ../index.php"); exit;

}elseif (!empty($erroTamanho) || !empty($erroExtensao)) {
  echo "$erroTamanho";
  echo "<br>";
  echo "$erroExtensao";

}else{

  try{
      //Estava mandando os dados por JSON, porém não dá para mandar imagem pelo JSON
      //$json = $_POST;
		  //$dados = json_decode(json_encode($json), true);

  include 'conexao/conecta.php';

  if (isset($_POST['idAt'])) {

    $show_id = $_POST['idAt'];
    $show_nome = $_POST['nomeAt'];
    $show_local = $_POST['enderecoAt'];
    $show_contato = $_POST['contatoAt'];
    $show_preco = $_POST['precoAt'];
    $estilo_show_id = $_POST['estiloAt'];
    $show_liberado = 0;

    if (!isset($_POST['alterarData'])) {
      $show_data = $_POST['dateAt'];

      $sql = $pdo->prepare("update shows  SET show_nome = ?, show_data = ?, show_local = ?, show_contato = ?, show_preco = ?, show_liberado = ?, estilo_show_id = ? WHERE show_id = ?");
      $sql->bindParam(1, $show_nome , PDO::PARAM_STR);
      $sql->bindParam(2, $show_data , PDO::PARAM_STR);
      $sql->bindParam(3, $show_local , PDO::PARAM_STR);
      $sql->bindParam(4, $show_contato , PDO::PARAM_STR);
      $sql->bindParam(5, $show_preco , PDO::PARAM_STR);
      $sql->bindParam(6, $show_liberado , PDO::PARAM_INT);
      $sql->bindParam(7, $estilo_show_id , PDO::PARAM_INT);
      $sql->bindParam(8, $show_id , PDO::PARAM_INT);
      $sql->execute();

    }elseif (isset($_POST['alterarData'])) {
      $sql = $pdo->prepare("update shows  SET show_nome = ?, show_local = ?, show_contato = ?, show_preco = ?, show_liberado = ?, estilo_show_id = ? WHERE show_id = ?");
      $sql->bindParam(1, $show_nome , PDO::PARAM_STR);
      $sql->bindParam(2, $show_local , PDO::PARAM_STR);
      $sql->bindParam(3, $show_contato , PDO::PARAM_STR);
      $sql->bindParam(4, $show_preco , PDO::PARAM_STR);
      $sql->bindParam(5, $show_liberado , PDO::PARAM_INT);
      $sql->bindParam(6, $estilo_show_id , PDO::PARAM_INT);
      $sql->bindParam(7, $show_id , PDO::PARAM_INT);
      $sql->execute();
    }

    $count = $sql->rowCount();

    if ($count == 1) {
      if (!empty($_FILES['inputImagem_At'])) {
        $idFotoShow = $show_id; 
      //Pegar ID da tabela para atribuir para imagem respectiva
        $new_name = $idFotoShow . $ext; 
        //Código para mover a foto
        move_uploaded_file($_FILES['inputImagem_At']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
        
        
      }
      echo "1";

    }else{
      if (!empty($_FILES['inputImagem_At'])) {
        $idFotoShow = $show_id; 
      //Pegar ID da tabela para atribuir para imagem respectiva
        $new_name = $idFotoShow . $ext; 
        //Código para mover a foto
        move_uploaded_file($_FILES['inputImagem_At']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
      }

      echo "1";
      //Se não houver nenhuma linha alterada, significa que o usuário não alterou nenhum campo de texto,
      //e o mysql não modificou nenhuma linha do banco. Qualquer erro de execução do SQL será pego no TRY CATCH
    }

      }else{//Fim Primeiro IF
        echo "Ocorreu um erro no envio dos dados. Tente novamente.";
      }

      

    }
    catch(PDOException $e){
    	echo $e->getCode();
    }

  }
}



  ?>