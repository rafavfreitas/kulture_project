<?php

include "conexao/conecta.php";

try {
	$consulta = $pdo->prepare("select show_nome from shows order by show_nome;");
	$consulta->execute();

	echo "<input list='enter' name='enter' class='form-control input-md' placeholder='Buscar'>";
	echo "<datalist id='enter'>";
	while ($linha = $consulta->fetch(PDO::FETCH_OBJ)) {
		echo "<option value='".$linha->show_nome."'>";
	}
	echo "</datalist>";
} catch (PDOException $e) {
	echo 'ERROR: ' . $e->getCode(). ' DataList';
}


?>

