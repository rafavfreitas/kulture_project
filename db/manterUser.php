<?php

require_once ("validaSessao.php");
if (empty($_POST) AND (empty($_POST['nome']) OR empty($_POST['email']) OR empty($_POST['telefone']) OR empty($_POST['login'])  )) {
    header("Location: ../index.php"); exit;
}else{

    try{
   

      $user_id = $_SESSION['UsuarioID'];
      include 'conexao/conecta.php';

      $user_nome = $_POST['nome'];
      $user_email = $_POST['email'];
      $user_telefone = $_POST['telefone'];
      $user_login = $_POST['login'];


      if ( isset($_POST['senha']) && !isset($_POST['datenasc']) ){
        $user_senha = $_POST['senha'];

        $sql = $pdo->prepare("update usuarios  SET user_nome = ?, user_email = ?, user_telefone = ?, user_login = ?, user_senha = sha(?) WHERE user_id = ?");
        $sql->bindParam(1, $user_nome , PDO::PARAM_STR);
        $sql->bindParam(2, $user_email , PDO::PARAM_STR);
        $sql->bindParam(3, $user_telefone , PDO::PARAM_STR);
        $sql->bindParam(4, $user_login , PDO::PARAM_STR);
        $sql->bindParam(5, $user_senha , PDO::PARAM_STR);
        $sql->bindParam(6, $user_id , PDO::PARAM_INT);
        $sql->execute();

      }elseif ( isset($_POST['datenasc']) && !isset($_POST['senha']) ){
        $user_datanasc = $_POST['datenasc'];

        $sql = $pdo->prepare("update usuarios  SET user_nome = ?, user_email = ?, user_telefone = ?, user_login = ?, user_datanasc = ? WHERE user_id = ?");
        $sql->bindParam(1, $user_nome , PDO::PARAM_STR);
        $sql->bindParam(2, $user_email , PDO::PARAM_STR);
        $sql->bindParam(3, $user_telefone , PDO::PARAM_STR);
        $sql->bindParam(4, $user_login , PDO::PARAM_STR);
        $sql->bindParam(5, $user_datanasc , PDO::PARAM_STR);
        $sql->bindParam(6, $user_id , PDO::PARAM_INT);
        $sql->execute();
      

      }elseif ( isset($_POST['datenasc']) && isset($_POST['senha']) ){
        $user_senha = $_POST['senha'];
        $user_datanasc = $_POST['datenasc'];

        $sql = $pdo->prepare("update usuarios  SET user_nome = ?, user_email = ?, user_telefone = ?, user_login = ?, user_senha = sha(?), user_datanasc = ? WHERE user_id = ?");
        $sql->bindParam(1, $user_nome , PDO::PARAM_STR);
        $sql->bindParam(2, $user_email , PDO::PARAM_STR);
        $sql->bindParam(3, $user_telefone , PDO::PARAM_STR);
        $sql->bindParam(4, $user_login , PDO::PARAM_STR);
        $sql->bindParam(5, $user_senha , PDO::PARAM_STR);
        $sql->bindParam(6, $user_datanasc , PDO::PARAM_STR);
        $sql->bindParam(7, $user_id , PDO::PARAM_INT);
        $sql->execute();
       

      }else{
        $sql = $pdo->prepare("update usuarios  SET user_nome = ?, user_email = ?, user_telefone = ?, user_login = ? WHERE user_id = ?");
        $sql->bindParam(1, $user_nome , PDO::PARAM_STR);
        $sql->bindParam(2, $user_email , PDO::PARAM_STR);
        $sql->bindParam(3, $user_telefone , PDO::PARAM_STR);
        $sql->bindParam(4, $user_login , PDO::PARAM_STR);
        $sql->bindParam(5, $user_id , PDO::PARAM_INT);
        $sql->execute();

      }

      $count = $sql->rowCount();
      if ($count == 1) {
        $sql = $pdo->prepare("update interesse_show  SET int_show_ativo = 0 WHERE user_id = ?");
        $sql->bindParam(1, $user_id , PDO::PARAM_STR);
        $sql->execute();

        $sql = $pdo->prepare("update interesse_fest  SET int_fest_ativo = 0 WHERE user_id = ?");
        $sql->bindParam(1, $user_id , PDO::PARAM_STR);
        $sql->execute();

        if (isset($_POST['estiloShow_list'])) {
          foreach($_POST['estiloShow_list'] as $selected) {
          
            $sql = $pdo->prepare("update interesse_show  SET int_show_ativo = 1 WHERE user_id = ? and estilo_show_id = ?");
            $sql->bindParam(1, $user_id , PDO::PARAM_STR);  
            $sql->bindParam(2, $selected , PDO::PARAM_STR);
            $sql->execute();

          }
        }

        if (isset($_POST['estiloFest_list'])) {
          foreach($_POST['estiloFest_list'] as $selected) {
            
            $sql = $pdo->prepare("update interesse_fest  SET int_fest_ativo = 1 WHERE user_id = ? and estilo_fest_id = ?");
            $sql->bindParam(1, $user_id , PDO::PARAM_STR);  
            $sql->bindParam(2, $selected , PDO::PARAM_STR);
            $sql->execute();

          }
        }

        

        echo "1";

      }else{
        //Se não houver nenhuma linha alterada, significa que o usuário não alterou nenhum campo de texto,
        //e o mysql não modificou nenhuma linha do banco. Qualquer erro de execução do SQL será pego no TRY CATCH

        $sql = $pdo->prepare("update interesse_show  SET int_show_ativo = 0 WHERE user_id = ?");
        $sql->bindParam(1, $user_id , PDO::PARAM_STR);
        $sql->execute();

        $sql = $pdo->prepare("update interesse_fest  SET int_fest_ativo = 0 WHERE user_id = ?");
        $sql->bindParam(1, $user_id , PDO::PARAM_STR);
        $sql->execute();

        if (isset($_POST['estiloShow_list'])) {
          foreach($_POST['estiloShow_list'] as $selected) {
          
            $sql = $pdo->prepare("update interesse_show  SET int_show_ativo = 1 WHERE user_id = ? and estilo_show_id = ?");
            $sql->bindParam(1, $user_id , PDO::PARAM_STR);  
            $sql->bindParam(2, $selected , PDO::PARAM_STR);
            $sql->execute();

          }
        }

        if (isset($_POST['estiloFest_list'])) {
          foreach($_POST['estiloFest_list'] as $selected) {
            
            $sql = $pdo->prepare("update interesse_fest  SET int_fest_ativo = 1 WHERE user_id = ? and estilo_fest_id = ?");
            $sql->bindParam(1, $user_id , PDO::PARAM_STR);  
            $sql->bindParam(2, $selected , PDO::PARAM_STR);
            $sql->execute();

          }
        }

        echo "1";

      }

      }catch(PDOException $e){
        echo "Erro de Inserção: ";
        //echo $e->getCode();
        echo $e;

      }


}

  ?>