<?php

include "conexao/conecta.php";

try {
	$consulta = $pdo->prepare("select fest_nome from festivais order by fest_nome;");
	$consulta->execute();

	echo "<input list='enter' name='enter' class='form-control input-md' placeholder='Buscar'>";
	echo "<datalist id='enter'>";
	while ($linha = $consulta->fetch(PDO::FETCH_OBJ)) {
		echo "<option value='".$linha->fest_nome."'>";
	}
	echo "</datalist>";
} catch (PDOException $e) {
	echo 'ERROR: ' . $e->getCode(). ' DataList';
}


?>

