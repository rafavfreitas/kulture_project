<?php
include 'conexao/conecta.php';
require_once ("validaSessaoAdmin.php"); 

$user_id = $_SESSION['UsuarioID'];

    try{

        $sql = $pdo->prepare("select user_nome, user_email, user_telefone, user_datanasc, user_login FROM usuarios where user_id = ?;");
        $sql->bindParam(1, $user_id , PDO::PARAM_STR);
        $sql->execute();
        $linha = $sql->fetch(PDO::FETCH_ASSOC);


        $user_nome = $linha['user_nome'];
        $user_email = $linha['user_email'];
        $user_telefone = $linha['user_telefone'];
        $user_datanasc = $linha['user_datanasc'];
        $user_login = $linha['user_login'];
        
        $sql = $pdo->prepare("select i.int_show_ativo, i.estilo_show_id, e.estilo_show_nome  from interesse_show i inner join estilo_show e on i.estilo_show_id = e.estilo_show_id where user_id = ? order by int_show_id;");
        $sql->bindParam(1, $user_id , PDO::PARAM_STR);
        $sql->execute();

        $sqlFest = $pdo->prepare("select i.int_fest_ativo, i.estilo_fest_id, e.estilo_fest_nome  from interesse_fest i inner join estilo_festival e on i.estilo_fest_id = e.estilo_fest_id where user_id = ? order by int_fest_id;");
        $sqlFest->bindParam(1, $user_id , PDO::PARAM_STR);
        $sqlFest->execute();



    }catch(PDOException $e) {
        //echo 'ERROR: ' . $e->getCode();
        echo $e;
    }


?>