<?php

  session_start();  //A seção deve ser iniciada em todas as páginas
  if (!isset($_SESSION['UsuarioID']) || !isset($_SESSION['UsuarioNome']) || !isset($_SESSION['UsuarioNivel'])  ) {
    session_destroy();            //Destroi a seção por segurança
    header("Location: ../index.php"); exit;  //Redireciona o visitante para login
  }else {
    $nivel_necessario = 1;
    if ($_SESSION['UsuarioNivel'] != $nivel_necessario ) {
      session_destroy();
      header("Location: ../index.php"); exit;
    }
  }

  
  ?>