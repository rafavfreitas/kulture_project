<?php

if (!empty($_POST['show_id_pub'])) {
	try{
		$show_id_pub = $_POST["show_id_pub"];
		include 'conexao/conecta.php';

		$sql = $pdo->prepare("update shows SET show_liberado = 1 WHERE show_id = ?");
		$sql->bindParam(1, $show_id_pub , PDO::PARAM_STR);
		$sql->execute();

		$sql = $pdo->prepare("select estilo_show_id, user_id from shows where show_id = ?");
		$sql->bindParam(1, $show_id_pub , PDO::PARAM_STR);
		$sql->execute();

		$linha = $sql->fetch(PDO::FETCH_OBJ);
		$estilo_id = $linha->estilo_show_id;
		$divulgadorId = $linha->user_id;

		$sql = $pdo->prepare("select user_email from usuarios where user_id = ?");
		$sql->bindParam(1, $divulgadorId , PDO::PARAM_STR);
		$sql->execute();
		$linha = $sql->fetch(PDO::FETCH_OBJ);
		$emailDivulgador = $linha->user_email;

		$sql = $pdo->prepare("select u.user_email, u.user_nome, i.estilo_show_id, es.estilo_show_nome from usuarios u inner join interesse_show i on u.user_id= i.user_id inner join estilo_show es on i.estilo_show_id = es.estilo_show_id where i.estilo_show_id = ? and i.int_show_ativo = 1");
		$sql->bindParam(1, $estilo_id , PDO::PARAM_STR);
		$sql->execute();
		

		
			require_once('../db/class/class.phpmailer.php');

			try {

    		$mail = new PHPMailer(true);// Inicia a classe PHPMailer
    		$mail->CharSet = 'UTF-8';
			$mail->IsSMTP(); // Define que a mensagem será SMTP
			$mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
			$mail->SMTPSecure = 'tls';
			$mail->Host = 'smtp.gmail.com'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
			$mail->Port       = 587; //  Usar 587 porta SMTP
			$mail->Username = 'projetokuture@gmail.com'; // Usuário do servidor SMTP (endereço de email)
			$mail->Password = 'kuture123'; // Senha do servidor SMTP (senha do email usado)
			$mail->SetFrom('projetokuture@gmail.com' , 'Sistema Kuture'); //Seu e-mail

			$body = file_get_contents('emails/email_publicacao.html');

			while ($linha = $sql->fetch(PDO::FETCH_OBJ)) {
				$email = $linha->user_email;
				$primeiroNome = explode(" ", $linha->user_nome); //Pegar apenas o primeiro nome
				$mail->Subject  =   "Novo Evento Publicado";
				$mail->AddAddress("$email");
				
				$body = str_replace('%primeiroNome%', $primeiroNome[0], $body); //Substituir variável dentro do HTML
				$body = str_replace('%estiloNome%', $linha->estilo_show_nome, $body); //Substituir variável dentro do HTML
				$mail->MsgHTML($body);
				$mail->IsHTML(true); // send as HTML
				$enviarParaUsuario = $mail->Send();//Enviar E-Mail
				$mail->clearAddresses();
    			$mail->clearAttachments();
			    
				
			}	

				$body = file_get_contents('emails/email_divulgador.html');
				$mail->Subject  =   "Seu evento foi publicado";
				$mail->AddAddress("$emailDivulgador");
				$mail->MsgHTML($body);
				$mail->IsHTML(true); // send as HTML
				$enviarParaUsuario = $mail->Send();//Enviar E-Mail
				$mail->clearAddresses();
    			$mail->clearAttachments();

				if ($enviarParaUsuario) {
				    echo '1';
				} else {
				    echo '2';
				}

			}catch (phpmailerException $e) {
			    	//echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
			    	echo "Você foi cadastrado, mas não conseguimos te enviar um e-mail de confirmação.";
			}

		

	}catch(PDOException $e){
	 	//echo $e->getCode();
	 	echo $e;
	}

//Deletar Clientes
			}elseif (!empty($_POST['deletar_show_id'])) {
				try{
					$show_id = $_POST["deletar_show_id"];
					include 'conexao/conecta.php';

					$sql = $pdo->prepare("delete FROM shows WHERE show_id = ?");
					$sql->bindParam(1, $show_id , PDO::PARAM_INT);
					$sql->execute();

					$count = $sql->rowCount();

					if ($count == 1) {
						echo "1";
					}else{
						echo "Ocorreu um ERRO na execução da instrução!";
					}   

				}
				catch(PDOException $e){
					echo $e->getCode();
				}	


//Atualizar dados dos Clientes
			}

			?>