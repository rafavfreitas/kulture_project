<?php
if (empty($_POST) AND (empty($_POST['nome']) OR empty($_POST['email']) OR empty($_POST['comentario']) )) {
    header("Location: ../index.php"); exit;
}else{

	$nome = $_POST["nome"];
	$email = $_POST['email'];
	$comentario = $_POST["comentario"];

	try {
		include 'conexao/conecta.php';
		$sql = $pdo->prepare("insert into contatos (contato_nome, contato_email, contato_comentario, contato_dataContato) values (?,?,?,NOW())");

		$sql->bindParam(1, $nome , PDO::PARAM_STR);
		$sql->bindParam(2, $email , PDO::PARAM_STR);
		$sql->bindParam(3, $comentario , PDO::PARAM_STR);
		$sql->execute();
		$count = $sql->rowCount();


		if ($count == 1) {

	 		require_once('class/class.phpmailer.php'); 

		    try {
		 		
				$mail = new PHPMailer(true);// Inicia a classe PHPMailer
				$mail->CharSet = 'UTF-8';
				$mail->IsSMTP(); // Define que a mensagem será SMTP
				$mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
				$mail->SMTPSecure = 'tls';
				$mail->Host = 'smtp.gmail.com'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
		     	$mail->Port       = 587; //  Usar 587 porta SMTP
		     	$mail->Username = 'projetokuture@gmail.com'; // Usuário do servidor SMTP (endereço de email)
		     	$mail->Password = 'kuture123'; // Senha do servidor SMTP (senha do email usado)
		 
		     	//Define o remetente
		     	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
		     	$mail->SetFrom('projetokuture@gmail.com' , 'Sistema Kuture'); //Seu e-mail
		     	//$mail->AddReplyTo('seu@e-mail.com.br', 'Nome'); //Seu e-mail
		     	$mail->Subject = 'Um usuário entrou em contato conosco.';//Assunto do e-mail
		      	//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

		      	//Define os destinatário(s)
		     	$mail->AddAddress('projetokuture@gmail.com', 'Contato');
			    //Campos abaixo são opcionais 
			    //$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
			    //$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
			    //$mail->AddAttachment('images/phpmailer.gif'); // Adicionar um anexo
				//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

				//Define o corpo do email
		     	$body = file_get_contents('emails/email_noreply_dados.html');
				$body = str_replace('%nome%', $nome, $body);
				$body = str_replace('%email%', $email, $body);
				$body = str_replace('%comentario%', $comentario, $body);
				$mail->MsgHTML($body);//Define o corpo do e-mail, pode colocar html diretamente aqui.
				$mail->IsHTML(true); // send as HTML
			    $enviarParaKuture = $mail->Send();

			    //$mail->MsgHTML(file_get_contents('email_noreply.html', dirname(__FILE__))); //Enviar um arquivo html já pronto
			    //$mail->AltBody = $mail->Body; //Alternativa caso o arquivo não seja carregado
			    //$mail->AltBody = "Recebemos seu contato, retornaremos em breve" //Alternativa caso o arquivo não seja carregado
			    /***************************************************************************/

			    
			    $mail->ClearAllRecipients(); //Limpar todos os que destinatiarios: TO, CC, BCC Clear addresses of all types
			    $primeiroNome = explode(" ", $nome); //Pegar apenas o primeiro nome
			    $mail->Subject  =   "Recebemos seu contato";
			    $mail->AddAddress("$email");
				$body = file_get_contents('emails/email_noreply.html');
				$body = str_replace('%primeiroNome%', $primeiroNome[0], $body); //Substituir variável dentro do HTML
				$mail->MsgHTML($body);
				$mail->IsHTML(true); // send as HTML
			    $enviarParaCliente = $mail->Send();//Enviar E-Mail

				if ($enviarParaCliente && $enviarParaKuture) {
				      echo '1';
				} else {
				       echo '2';
				}

			}catch (phpmailerException $e) {
		    	echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
		    	echo "Erro do PHPMailer";
			}


		}else {
    		echo "0";//Erro de inserção no banco.
  		}


		
	} catch (Exception $erro01) {
		echo "$erro01";
	}
	
  
}//Fim 1º ELSE




?>