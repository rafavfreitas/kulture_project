<?php

require_once ("validaSessao.php");

$user_id = $_SESSION['UsuarioID'];

if (!empty($_FILES['inputImagem_fest'])) {
	//ini_set("upload_max_filesize", "5M");
	
	
	$dir = '../imagens/img_eventos/festivais/'; //Diretório para uploads
	$target_file = $dir . basename($_FILES["inputImagem_fest"]["name"]);
	$extensao = pathinfo($target_file,PATHINFO_EXTENSION);
	$ext = "." . $extensao;
	//$ext = strtolower(substr($_FILES['inputImagem_fest']['name'],-4)); //Pegando extensão do arquivo
    //$ext = '.jpg'; //Definir a extensão do arquivo
    //$new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo

	$erroTamanho = "";
	$erroExtensao = "";
											  
    if ($_FILES["inputImagem_fest"]["size"] > 1024000) {
    	//upload_max_filesize
		$erroTamanho = "Arquivo acima do limite, favor escolher um arquivo menor";
	}

	if($extensao != "jpg" && $extensao != "png" && $extensao != "jpeg" && $extensao != "gif" ) {
	    $erroExtensao = "Somente serão aceitos arquivos JPG, JPEG, PNG & GIF.";
	    
	}
    
}if (empty($_POST) AND (empty($_POST['nome_fest']) OR empty($_POST['date_fest']) OR empty($_POST['local_fest']) OR empty($_POST['telefone_fest']) OR empty($_POST['precos_fest']) OR empty($_POST['estilo_fest']) OR empty($_FILES['inputImagem_fest']) )) {
    header("Location: ../index.php"); exit;

}elseif (!empty($erroTamanho) || !empty($erroExtensao)) {
	echo "$erroTamanho";
	echo "<br>";
	echo "$erroExtensao";

}else{

	$nome_fest = $_POST["nome_fest"];
	$date_fest = $_POST['date_fest'];
	$local_fest = $_POST["local_fest"];
	$telefone_fest = $_POST["telefone_fest"];
	$precos_fest = $_POST["precos_fest"];
	$estilo_fest = $_POST["estilo_fest"];
	$fest_liberado = 0;

	try {
		include 'conexao/conecta.php';

		$sql = $pdo->prepare("insert into festivais (fest_nome, fest_data, fest_local, fest_contato, fest_preco, fest_publicado, fest_liberado, estilo_fest_id, user_id) values (?,?,?,?,?,NOW(),?,?,?);");

		$sql->bindParam(1, $nome_fest , PDO::PARAM_STR);
		$sql->bindParam(2, $date_fest , PDO::PARAM_STR);
		$sql->bindParam(3, $local_fest , PDO::PARAM_STR);
		$sql->bindParam(4, $telefone_fest , PDO::PARAM_STR);
		$sql->bindParam(5, $precos_fest , PDO::PARAM_STR);
		$sql->bindParam(6, $fest_liberado , PDO::PARAM_INT);
		$sql->bindParam(7, $estilo_fest , PDO::PARAM_INT);
		$sql->bindParam(8, $user_id , PDO::PARAM_INT);
		$sql->execute();

		$idFotofest = $pdo->lastInsertId(); 
		//Pegar ID da tabela para atribuir para imagem respectiva
		$new_name = $idFotofest . $ext; 
		//Código para mover a foto
		move_uploaded_file($_FILES['inputImagem_fest']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
		
		echo "1";

	} catch (Exception $erro01) {
		//echo "Erro: $erro01";
		echo "Erro PDO: Não foi possivel Cadastrá-lo na base de dados, tente novamente.";
	}
	
  
}//Fim 1º ELSE




?>