Versão 1.0

O Projeto Kulture é uma plataforma completa para inscrições de eventos para divulgação de shows e festivais do Recife. 
A ideia é facilitar o trabalho do profissional de eventos que deseja divulgar seus grandes eventos no recife ou de pessoas que gostariam de começar a divulgar seus eventos de uma forma gratuita e com grande poder de alcance

Existem dois tipos de usuários no sistema, o administrador e usuário comum. O administrador é previamente cadastrado na instalação da ferramenta. Porém os usuários comuns são cadastrados por qualquer um que deseja utilizar o sistema.

Os usuários comuns poderão criar eventos, e seguir determinados estilos previamente cadastrados. Ele pode ver seus eventos cadastrados, editá-los e paga-los. Para qualquer alteração nos eventos, uma permissão precisará acontecer antes da alteração ser publicada.