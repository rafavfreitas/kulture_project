
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Cadastro</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript"></script>
  
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

  <nav class="navbar navbarIndex navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="imagens/logos/logo4.2.png"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navIndex navbar-right">
          <li><a href="index.php"><i class="fa fa-reply"></i> VOLTAR</a></li>
          <li class="dropdown">
            <a class="dropdown-toggle menuDrop" data-toggle="dropdown" href="#">EVENTOS
              <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">FESTIVAIS</li>
                <li><a href="festivais.php?ref=1">DANÇA</a></li>
                <li><a href="festivais.php?ref=2">MÚSICA</a></li>
                <li><a href="festivais.php?ref=3">TEATRO</a></li>
                <li><a href="festivais.php?ref=4">OUTROS</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">SHOWS</li>
                <li><a href="shows.php?ref=1">AXÉ</a></li>
                <li><a href="shows.php?ref=2">BOSSA NOVA</a></li>
                <li><a href="shows.php?ref=3">FORRÓ</a></li> 
                <li><a href="shows.php?ref=4">FUNK</a></li>
                <li><a href="shows.php?ref=5">GOLSPEL</a></li>
                <li><a href="shows.php?ref=6">HIP HOP</a></li>
                <li><a href="shows.php?ref=7">JAZZ</a></li>
                <li><a href="shows.php?ref=8">MPB</a></li>
                <li><a href="shows.php?ref=9">PAGODE</a></li>
                <li><a href="shows.php?ref=10">RAP</a></li>
                <li><a href="shows.php?ref=11">REGGAE</a></li>
                <li><a href="shows.php?ref=12">ROCK</a></li>
                <li><a href="shows.php?ref=13">SAMBA</a></li>
                <li><a href="shows.php?ref=14">SERTANEJO</a></li>
                <li><a href="shows.php?ref=15">Outros</a></li>
              </ul>
            </li>
            <li class="active"><a href="cadastro.php">CADASTRE-SE</a></li>
            <li id="myLogin" style="cursor:pointer;cursor:hand;"><a><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid divImgHand">
     <img class="imgHand" src="imagens/carousel/hand.jpg">

   </div>
   <div class="modal fade" id="modalLogin" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="modalTitulo"><span class="glyphicon glyphicon-home"></span> Bem Vindo!</h4>
          <p style="font-size: 12pt;">Área do Usuário Kulture.</p>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <div class="divImgModal">
            <img id="imgModal" src="imagens/logos/logo2.png">
          </div>
          <form role="form" id="formLogin">
            <div class="form-group">
              <label for="username"><span class="glyphicon glyphicon-user"></span> Usuário</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Informe seu Login">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Senha</label>
              <input type="password" class="form-control" id="psw" name="psw" placeholder="Informe sua senha">
            </div>
            <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Acessar</button>
            <div id="divGif"><img  id="loginGif" src="imagens/gif/load.gif" style="max-width: 50px;"></div>
            <p class="text-center" id="erroLogin"></p>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
          <p>Não é cadastrado?<a href="cadastro.php">Registre-se.</a></p>
          <!--p>Esqueceu a <a href="#">Senha?</a></p-->
        </div>
      </div>
      
    </div>
  </div> 

  <div class="container containerCadastro">

    <div class="panel panel-default panelCadastro">
      <div class="panel-heading panelCadastroHeading">
       <h4 id="cadastroTitulo" class="text-center">
        <span class="glyphicon glyphicon-ok iconCadasto"></span> Obrigado!
      </h4>
      <p class="text-center"><span class="glyphicon glyphicon-list-alt iconCadasto"></span> Preencha este formulário abaixo.</p>
      </div>

      <div class="divGifCadastro">
        <img id="gifCadastro" src="imagens/gif/equalizer.gif">
      </div>

    <form class="form-horizontal" id="formCadastro">
      <fieldset id="fieldsetCadastro">
        <!--Nome-->
        <div class="form-group">
          <label class="control-label col-sm-2" for="nome">Nome:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe seu nome" required>
          </div>
        </div>

        <!--E-Mail-->
        <div class="form-group">
          <label class="control-label col-sm-2" for="email">E-Mail:</label>
          <div class="col-sm-10"> 
            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail para contato." pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="exemplo@exemplo.com" required>
          </div>
        </div>

        <!--Telefone-->
        <div class="form-group">
          <label class="control-label col-sm-2" for="telefone">Telefone:</label>
          <div class="col-sm-10">
            <input pattern="^\d{2}-\d{5}-\d{4}$" type="tel" class="form-control" rows="3" id="telefone" name="telefone" OnKeyPress="formatar('##-#####-####', this)" maxlength="13" placeholder="00-00000-0000" style="max-width: 200px;" required></input>
            Caso deseje informar um Nº Fixo, colocar um 0 no lugar do 9º dígito.
          </div>
        </div>

        <!--Login-->
        <div class="form-group">
         <label class="control-label col-sm-2" for="login">Login:</label>
         <div class="col-sm-10"> 
           <input type="text" class="form-control" id="login" name="login" placeholder="Informe um Login de acesso" required>

         </div>
       </div>
       <!--Senha-->
       <div class="form-group">
        <label class="control-label col-sm-2" for="senha">Senha:</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="senha" name="senha" placeholder="Informe sua senha" required>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2" for="nome">Data Nasc.</label>
        <div class="col-sm-10">
          <input class="form-control" type="date" id="datenasc" name="datenasc" style='max-width: 180px;' required>
          <p>* Para caso você esqueça sua senha.<br></p>

        </div>
      </div>
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
         <button type="submit" class="btn btn-default">Cadastrar</button>
         <img  id="loadCadastro" src="imagens/gif/load1.gif" style="max-width: 50px;">
       </div>
     </div>
     <p class="text-center animated bounceInRight" id="sucessoCadastro"><span class="glyphicon glyphicon-ok"></span><strong> Usuário cadastrado e liberado para acessar a ferramenta. Você já poderá realizar o Login.<br>Lá você escolherá os estilos que deseja seguir.</strong></p>
     <p class="text-center" id="erroCadastro"></p><br>
   </fieldset>
 </form>
 <div class="panel-footer footerCadastro">
  <p class="text-center">Obrigado pelo interesse em nossa feramenta.</p>
</div>
</div>

</div>

<?php require_once ("footer.php"); ?>
<script async src="js/main.js"></script>

</body>
</html>
