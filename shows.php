<?php include 'db/timelineShows.php';?>
<!DOCTYPE html>
<html lang="pt">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Shows</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Kulture é uma plataforma completa para inscrições de eventos para divulgação da shows e festivais do Recife. Nossas soluções simplificam a forma de administrar as etapas do evento, trazem vantagens para o organizador dos eventos e para as pessoas que gostam/curtem esses tipos de eventos. ">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Kulture, eventos, Recife, Festivais, Divulgação">
  <meta name="author" content="Equipe Kulture">

  <link rel="icon" href="imagens/logos/ico.ico" type="image/x-icon">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript"></script>
  
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbarIndex navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="imagens/logos/logo4.2.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navIndex navbar-right">
        <li><a href="index.php"><i class="fa fa-reply"></i> VOLTAR</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle menuDrop" data-toggle="dropdown" href="#">EVENTOS
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="dropdown-header">FESTIVAIS</li>
              <li><a href="festivais.php?ref=1">DANÇA</a></li>
              <li><a href="festivais.php?ref=2">MÚSICA</a></li>
              <li><a href="festivais.php?ref=3">TEATRO</a></li>
              <li><a href="festivais.php?ref=4">OUTROS</a></li>
              <li role="separator" class="divider"></li>
            <li class="dropdown-header">SHOWS</li>
              <li><a href="shows.php?ref=1">AXÉ</a></li>
              <li><a href="shows.php?ref=2">BOSSA NOVA</a></li>
              <li><a href="shows.php?ref=3">FORRÓ</a></li> 
              <li><a href="shows.php?ref=4">FUNK</a></li>
              <li><a href="shows.php?ref=5">GOLSPEL</a></li>
              <li><a href="shows.php?ref=6">HIP HOP</a></li>
              <li><a href="shows.php?ref=7">JAZZ</a></li>
              <li><a href="shows.php?ref=8">MPB</a></li>
              <li><a href="shows.php?ref=9">PAGODE</a></li>
              <li><a href="shows.php?ref=10">RAP</a></li>
              <li><a href="shows.php?ref=11">REGGAE</a></li>
              <li><a href="shows.php?ref=12">ROCK</a></li>
              <li><a href="shows.php?ref=13">SAMBA</a></li>
              <li><a href="shows.php?ref=14">SERTANEJO</a></li>
              <li><a href="shows.php?ref=15">Outros</a></li>
          </ul>
        </li>
        <li><a href="cadastro.php">CADASTRE-SE</a></li>
        <li id="myLogin" style="cursor:pointer;cursor:hand;"><a><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron jumbIndexShows text-center">
    <img id="logo_maior" class="animated fadeIn" src="../imagens/logos/logo1.1.png" style="max-width: 50px;">
    <p id="textoLogo" class="animated fadeInUp">Amamos a música</p> 

</div>


<!-- Modal -->
  <div class="modal fade" id="modalLogin" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="modalTitulo"><span class="glyphicon glyphicon-home"></span> Bem Vindo!</h4>
          <p style="font-size: 12pt;">Área do Usuário Kulture.</p>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <div class="divImgModal">
            <img id="imgModal" src="imagens/logos/logo2.png">
          </div>
          <form role="form" id="formLogin">
            <div class="form-group">
              <label for="username"><span class="glyphicon glyphicon-user"></span> Usuário</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Informe seu Login">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Senha</label>
              <input type="password" class="form-control" id="psw" name="psw" placeholder="Informe sua senha">
            </div>
              <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Acessar</button>
              <div id="divGif"><img  id="loginGif" src="imagens/gif/load.gif" style="max-width: 50px;"></div>
              <p class="text-center" id="erroLogin"></p>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
          <p>Não é cadastrado?<a href="cadastro.php">Registre-se.</a></p>
          <p>Esqueceu a <a href="#">Senha?</a></p>
        </div>
      </div>
      
    </div>
  </div> 


<div class="container-fluid">
  <h2><?php echo $estilo_nome; ?></h2>
    <h3>Verifique os últimos eventos publicados</h3>
    <p><strong>Nota:</strong> Não achou o que procurava? <a href="index.php#contato">Fale conosco</a></p>
    
    
  
</div>

<div class="container containerViewShows">

<?php 
  try {
    foreach ($pdo->query($sql) as $row) {?>

    <div class="panel panel-default panelViewShowDefault">
      <div class="panel-heading panelViewShowHeading">
      <h3 class="text-center" style="font-size: 18pt;" class="panelViewShowTitulo"><?php echo $row['show_nome']; ?></h3>
      <?php echo "<img src='imagens/img_eventos/shows/".$row['show_id'].".jpg' class='img-responsive' style='margin-right: auto; margin-left: auto';>" ?>
      
      </div>

      <div class="panel-body">
      
      <div class="row">
        <div class="col-sm-6">
          <dl>
            <dt>Nome:</dt>
            <dd><?php echo $row['show_nome']; ?></dd>
            <dt>Data Show:</dt>
            <dd><?php echo $row['show_data']; ?></dd>
            <dt>Local:</dt>
            <dd><?php echo $row['show_local']; ?></dd>
          </dl>
        </div>
        <div class="col-sm-6">
          <dl>
            <dt>Contato:</dt>
            <dd><?php echo $row['show_contato']; ?></dd>
            <dt>Preço:</dt>
            <dd><?php echo $row['show_preco']; ?></dd>
            <dt>Publicado na ferramenta:</dt>
            <dd><?php echo $row['show_publicado']; ?></dd>
          </dl>
        </div>
      </div>

      </div>
    </div>
    <hr>
    <?php
    }
        } catch (Exception $e) {
          //echo 'ERROR: ' . $e->getCode() .'Formação das Linhas';
          echo $e;
        }?>


 
    <?php
                            //Verificar a pagina anterior e posterior
    $pagina_anterior = $pagina - 1;
    $pagina_posterior = $pagina + 1;
    ?>
                        <?php //Se estiver detro de uma pesquisa gera essa paginação
                        if (!empty($_GET['ref'])){?>
                        <nav class="text-center">
                          <ul class="pagination">
                            <li>
                              <?php
                              if($pagina_anterior != 0){ ?>
                              <a href="shows.php?ref=<?php echo $pesquisa;?>&pagina=<?php echo $pagina_anterior; ?>" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                              </a>
                              <?php }else{ ?>
                              <span aria-hidden="true">&laquo;</span>
                              <?php }  ?>
                            </li>
                            <?php 
                                    //Apresentar a paginacao
                            for($i = 1; $i < $num_pagina + 1; $i++){
                              if ($i == $pagina){
                                ?>
                                <li class="active"><a href="shows.php?ref=<?php echo $pesquisa;?>&pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                <?php }else{?>
                                <li><a href="shows.php?ref=<?php echo $pesquisa;?>&pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                                <?php } } ?>
                                <li>
                                  <?php
                                  if($pagina_posterior <= $num_pagina){ ?>
                                  <a href="shows.php?ref=<?php echo $pesquisa;?>&pagina=<?php echo $pagina_posterior; ?>" aria-label="Previous">
                                    <span aria-hidden="true">&raquo;</span>
                                  </a>
                                  <?php }else{ ?>
                                  <span aria-hidden="true">&raquo;</span>
                                  <?php }  ?>
                                </li>
                              </ul>
                            </nav>



                            <?php } ?>

</div>



<?php require_once ("footer.php"); ?>
<script async src="js/main.js"></script>

</body>
</html>
