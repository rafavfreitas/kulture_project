<nav class="navbar navbarIndex navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand animated fadeInLeft" href="#myPage"><img id="logo_menu" src="imagens/logos/logo4.2.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navIndex navbar-right">
        <li><a href="#sobre">SOBRE</a></li>
        <li><a href="#ferramenta">FERRAMENTA</a></li>
        <li><a href="#contato">CONTATO</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle menuDrop" data-toggle="dropdown" href="#">EVENTOS
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="dropdown-header">FESTIVAIS</li>
              <li><a href="festival.php">Nome do Festival</a></li>
              <li role="separator" class="divider"></li>
            <li class="dropdown-header">SHOWS</li>
              <li><a href="pagina-axe.php">AXÉ</a></li>
              <li><a href="pagina-bossa.php">BOSSA NOVA</a></li>
              <li><a href="pagina-forro.php">FORRÓ</a></li> 
              <li><a href="pagina-funk.php">FUNK</a></li>
              <li><a href="pagina-gospel.php">GOLSPEL</a></li>
              <li><a href="pagina-hiphop.php">HIP HOP</a></li>
              <li><a href="pagina-jazz.php">JAZZ</a></li>
              <li><a href="pagina-mpb.php">MPB</a></li>
              <li><a href="pagina-pagode.php">PAGODE</a></li>
              <li><a href="pagina-rap.php">RAP</a></li>
              <li><a href="pagina-reggae.php">REGGAE</a></li>
              <li><a href="pagina-rock.php">ROCK</a></li>
              <li><a href="pagina-samba.php">SAMBA</a></li>
          </ul>
        </li>
        <li><a href="cadastro.php#pagina">CADASTRE-SE</a></li>
        <li id="myLogin" style="cursor:pointer;cursor:hand;"><a><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
      </ul>
    </div>
  </div>
</nav>