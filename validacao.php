<?php

  // Verifica se houve POST e se o usuário ou a senha é(são) vazio(s)
  if (empty($_POST) AND (empty($_POST['login']) OR empty($_POST['psw']))) {
      header("Location: index.php"); exit;
  }

 
  $login = $_POST["login"];
  $senha = $_POST['senha'];


 try{
      include 'db/conexao/conecta.php';
      $sql = $pdo->prepare("select user_id, user_nome, user_nivel from usuarios WHERE (user_login = ?) and (user_senha = sha1(?) ) LIMIT 1 ");
      $sql->bindParam(1, $login , PDO::PARAM_STR);
      $sql->bindParam(2, $senha , PDO::PARAM_STR);
      $sql->execute();
      $count1 = $sql->rowCount();

      if ($count1 != 1) {
        echo "Login ou senha incorretos!";
      }
      else{
         if ($reg = $sql->fetch(PDO::FETCH_OBJ)) {
                  
          // Levanta a sessão 
          if (!isset($_SESSION)) session_start();
            //Salva os dados encontrados na sessão
            $_SESSION['UsuarioID'] = $reg->user_id;
            $_SESSION['UsuarioNome'] = $reg->user_nome;
            $_SESSION['UsuarioNivel'] = $reg->user_nivel;
            echo 1; //header("Location: /estoque/index.php"); exit;
       
          }//Fim IF
      }//Fim Else
   }//Fim TRY
   catch(PDOException $e){
      echo "Erro: ";
      echo $e->getMessage();
      //echo "\nPDO::errorCode(): ", $pdo->errorCode();
   }

?>